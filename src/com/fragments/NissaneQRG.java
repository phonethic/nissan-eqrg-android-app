package com.fragments;

import java.util.ArrayList;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnKeyListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import com.adapters.NissaneQRGListAdapter;
import com.adapters.NissaneqrgFeatureListAdapter;
import com.adapters.ViewPagerAdapter;
import com.phonethics.nissanqrg.R;


public class NissaneQRG extends Fragment {

	private ListView mlvNissanEqrg;
	private ListView mlvNissanOptions;

	private ViewFlipper mViewFlipper;

	private ViewPager mViewPager;

	private NissaneqrgFeatureListAdapter mNissanFeature;
	private ViewPagerAdapter mViewPagerAdapter;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(null);
	}


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.nissaneqrg, null);

		initViews(view);

		mlvNissanEqrg.setAdapter(new NissaneQRGListAdapter(getActivity()));
		//		mNissanFeature = new NissaneqrgFeatureListAdapter(getActivity());
		mViewPagerAdapter = new ViewPagerAdapter(getActivity(), 1);

		mlvNissanEqrg.setOnItemClickListener(clickListener);
		mlvNissanOptions.setOnItemClickListener(featureClick);

		return view;
	}


	private void initViews(View view){
		mlvNissanEqrg = (ListView) view.findViewById(R.id.lvNissanEqrg);
		mlvNissanOptions = (ListView) view.findViewById(R.id.lvNissanOption);

		mViewFlipper = (ViewFlipper) view.findViewById(R.id.viewFlipper);

		mViewPager = (ViewPager) view.findViewById(R.id.imagePager);
	}


	@Override
	public void onDestroyView() {
		super.onDestroyView();
	}


	@Override
	public void onDestroy() {
		super.onDestroy();
	}


	private OnItemClickListener clickListener = new OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> adapterView, View view, int position,
				long arg3) {

			//			switch (position) {
			//			case 0: {
			//mlvNissanOptions.setAdapter(mNissanFeature);
			mViewFlipper.showNext();

			//				break;
			//			}

			//			default:
			//				break;
			//			}


			if(position == 0){
				// for nissan terrano

				ArrayList<String> terrano = new ArrayList<String>();
				terrano.add("Terrano inside");
				terrano.add("Terrano leather");
				terrano.add("Terrano leather");
				mNissanFeature = new NissaneqrgFeatureListAdapter(getActivity(),terrano);
				mlvNissanOptions.setAdapter(mNissanFeature);

				Toast.makeText(getActivity(), "Terrano", 0).show();
			}
			else if(position == 1){
				// for nissan micra

				ArrayList<String> micra = new ArrayList<String>();
				micra.add("Micra inside");
				micra.add("Micra leather");
				micra.add("Micra interiors");
				mNissanFeature = new NissaneqrgFeatureListAdapter(getActivity(), micra);
				mlvNissanOptions.setAdapter(mNissanFeature);

				Toast.makeText(getActivity(), "Micra", 0).show();
			}
			else if(position == 2){
				// for nissan sunny

				ArrayList<String> sunny = new ArrayList<String>();
				sunny.add("Sunny inside");
				sunny.add("Sunny leather");
				sunny.add("Sunny interiors");
				mNissanFeature = new NissaneqrgFeatureListAdapter(getActivity(), sunny);
				mlvNissanOptions.setAdapter(mNissanFeature);

				Toast.makeText(getActivity(), "Sunny", 0).show();
			}

		}
	};


	private OnItemClickListener featureClick = new OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> adapterView, View view, int position,
				long arg3) {

			//			switch (position) {
			//			case 0: {

			//				
			//				mViewPager.setAdapter(mViewPagerAdapter);
			//				mViewFlipper.showNext();
			//				
			String str = ((TextView)view.findViewById(R.id.tvNissanFeatureName)).getText().toString();
			Toast.makeText(getActivity(), "" + str, 0).show();
			//				break;
			//			}
			
			//
			//			default:
			//				break;
			//			}

		}
	};


	private OnKeyListener keyListener = new OnKeyListener() {

		@Override
		public boolean onKey(View v, int keyCode, KeyEvent event) {
			if(keyCode == KeyEvent.KEYCODE_BACK){
				Toast.makeText(getActivity(), "Back press", Toast.LENGTH_SHORT).show();
			}
			return false;
		}
	};

}
