package com.fragments;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.adapters.TipsContactListViewAdapter;
import com.phonethics.nissanqrg.R;
import com.phonethics.nissanqrg.TipsGallery;

public class Tips extends Fragment {

	private ListView mLvTips;
	ProgressBar mProgressBar;

	TipsContactListViewAdapter mTadapter;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	/* Inflate the same view as ContactUs.java */
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.tips, null);

		initViews(view);

		mTadapter=new TipsContactListViewAdapter(getActivity(), 0, mProgressBar);
		mLvTips.setAdapter(mTadapter);

		mLvTips.setOnItemClickListener(listClickListener);
		return view;
	}


	@Override
	public void onDestroyView() {
		super.onDestroyView();
	}


	@Override
	public void onDestroy() {
		super.onDestroy();
	}


	private void initViews(View view){
		mLvTips = (ListView) view.findViewById(R.id.lvTips);
		mProgressBar = (ProgressBar) view.findViewById(R.id.progressBar);
	}

	OnItemClickListener listClickListener = new OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> adapterView, View view, int position,
				long arg3) {

			//			Toast.makeText(getActivity(), "Clicked " + position, 0).show();

			ArrayList<String>  imagesToPass = new ArrayList<String>();
			ArrayList<String>  vlinkToPass = new ArrayList<String>();
			ArrayList<String>  allID = new ArrayList<String>();

			ArrayList<String> tempLink=new ArrayList<String>();
			ArrayList<String> tempVLink=new ArrayList<String>();

			int allWaysSize = mTadapter.allWays.get(position).getIdilink().size();

			Log.d("Size of ","Size of " + allWaysSize);

			for(int k=0;k<allWaysSize;k++){

				Log.d("FRSTLINKS","FRSTLINKS" +  mTadapter.allWays.get(position).getIdilink().get(k));
				imagesToPass.add(mTadapter.allWays.get(position).getIdilink().get(k));
				vlinkToPass.add(mTadapter.allWays.get(position).getIdvlink().get(k));
				allID.add(mTadapter.allWays.get(position).getIdPhoto().get(k));

			}


			/*
			 *	Sorting photo Links 
			 */
			tempLink=sortLinks(allID, imagesToPass);
			imagesToPass.clear();
			for(int i=0;i<tempLink.size();i++)
			{
				imagesToPass.add(tempLink.get(i));

			}

			/*
			 *	Sorting video Links 
			 */
			tempVLink=sortLinks(allID, vlinkToPass);
			vlinkToPass.clear();
			for(int i=0;i<tempVLink.size();i++)
			{
				vlinkToPass.add(tempVLink.get(i));

			}


			Intent intent = new Intent(getActivity(), TipsGallery.class);
			intent.putStringArrayListExtra("imagesLink", imagesToPass);
			intent.putStringArrayListExtra("videoLinks", vlinkToPass);
			startActivity(intent);

		}
	};

	public ArrayList<String> sortLinks( ArrayList<String> id,ArrayList<String> link)
	{
		Map<Integer, String> valuesMap = new HashMap<Integer, String>();
		ArrayList<String> sortedList=new ArrayList<String>();

		for(int i=0;i<id.size();i++)
		{
			valuesMap.put(Integer.parseInt(id.get(i)), link.get(i));
		}

		Map<Integer, String> sortedMap = new TreeMap<Integer, String>(valuesMap);

		for (Iterator it = sortedMap.values().iterator(); it.hasNext();) {
			String value = it.next().toString();
			sortedList.add(value);
		}

		for(int i=0;i<sortedList.size();i++)
		{
			Log.i("-----------"+i+1,"----------------");
			Log.i("Links ", " "+sortedList.get(i));
		}

		return sortedList;

	}
}
