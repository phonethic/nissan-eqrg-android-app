package com.fragments;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.youtube.player.YouTubeApiServiceUtil;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubeStandalonePlayer;
import com.phonethics.nissanqrg.R;
import com.squareup.picasso.Picasso;

public class TipsPagerFragment extends Fragment{

	Context context;
	String imgLinks,videoLinks;
	private static final String DEVELOPER_KEY = "AIzaSyBKB8P9PyARG2hZKU4h9u58EesbqNzPuK4";

	public TipsPagerFragment()
	{

	}
	//	public TipsPagerFragment(Context context,String imgLinks,String videoLinks)
	//	{
	//		this.context=context;
	//		this.imgLinks=imgLinks;
	//		this.videoLinks=videoLinks;
	//
	//	}

	// newInstance constructor for creating fragment with arguments
	public static TipsPagerFragment newInstance(String imgLinks, String videoLinks) {
		TipsPagerFragment fragmentFirst = new TipsPagerFragment();
		Bundle args = new Bundle();
		args.putString("imgLinks", imgLinks);
		args.putString("videoLinks", videoLinks);
		fragmentFirst.setArguments(args);
		return fragmentFirst;
	}


	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		imgLinks = getArguments().getString("imgLinks");
		videoLinks = getArguments().getString("videoLinks");
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub


		View view = inflater.inflate(R.layout.tipspagerfragment, container, false);
		ImageView imgView = (ImageView) view.findViewById(R.id.tipsGallery);
		ProgressBar prog = (ProgressBar) view.findViewById(R.id.prog);
		Picasso.with(context)
		.load(imgLinks)
		.placeholder(R.drawable.placeholder)
		.error(R.drawable.placeholder)
		.into(imgView);


		imgView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if(!videoLinks.equals("")){

					try
					{
						if(YouTubeApiServiceUtil.isYouTubeApiServiceAvailable(getActivity()).equals(YouTubeInitializationResult.SUCCESS))
						{
							String vid=getYoutubeVideoId(videoLinks);
							Intent intent=YouTubeStandalonePlayer.createVideoIntent(
									getActivity(),DEVELOPER_KEY, vid, 0, true, false);
							startActivity(intent);
						}
						else
						{
							Toast.makeText(getActivity(), "Update or Install Youtube Player",Toast.LENGTH_SHORT).show();
							Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(videoLinks));
							startActivity(intent);
						}
					}catch(ActivityNotFoundException aex)
					{

					}
					catch(Exception ex)
					{
						ex.printStackTrace();
					}
				}
			}
		});

		return view;
	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
	}


	/*
	 * Getting id of video from url
	 */
	public static String getYoutubeVideoId(String youtubeUrl)
	{
		String video_id="";
		if (youtubeUrl != null && youtubeUrl.trim().length() > 0 && youtubeUrl.startsWith("http"))
		{

			String expression = "^.*((youtu.be"+ "\\/)" + "|(v\\/)|(\\/u\\/w\\/)|(embed\\/)|(watch\\?))\\??v?=?([^#\\&\\?]*).*"; // var regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/;
			CharSequence input = youtubeUrl;
			Pattern pattern = Pattern.compile(expression,Pattern.CASE_INSENSITIVE);
			Matcher matcher = pattern.matcher(input);
			if (matcher.matches())
			{
				String groupIndex1 = matcher.group(7);
				if(groupIndex1!=null && groupIndex1.length()==11)
					video_id = groupIndex1;
			}
		}
		return video_id;
	}
}
