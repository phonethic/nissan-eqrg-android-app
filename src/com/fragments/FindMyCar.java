package com.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Typeface;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.phonethics.nissanqrg.R;



public class FindMyCar extends Fragment implements LocationListener{

	private TextView mTvFind;
	private TextView mTvPark;
	private EditText mEtParkHint;

	private GoogleMap map;
	Location mLocation;
	LocationManager locationManager ;
	String provider;

	private double mLatitude,mLongitude, mSaved_lati, mSaved_longi;
	private  Editor mEditor ;

	public static final String SHARED_PREFERENCES_NAME = "save_lati_longi";
	public static final String LATITUDE = "latitude";
	public static final String LONGITUDE = "longitude";
	SharedPreferences 	mPrefs;
	SupportMapFragment fm;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		//		if(savedInstanceState!=null){
		//			pos = savedInstanceState.getInt("pos");
		//			context = getActivity();
		//		}
	}


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		ConnectivityManager cm 		= (ConnectivityManager)  getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo 		= cm.getActiveNetworkInfo();

		View view = inflater.inflate(R.layout.findmycar, null);

		initViews(view);

		Typeface type = Typeface.createFromAsset(getActivity().getResources().getAssets(),"Multicolore.otf"); 
		mTvFind.setTypeface(type);
		mTvPark.setTypeface(type);
		mEtParkHint.setTypeface(type);

		// Getting Google Play availability status
		int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getActivity());


		if(status!=ConnectionResult.SUCCESS){ // Google Play Services are not available

			int requestCode = 10;
			Dialog dialog = GooglePlayServicesUtil.getErrorDialog(status, getActivity(), requestCode);
			dialog.show();

		}else { // Google Play Services are available

			fm = (SupportMapFragment) getFragmentManager().findFragmentById(R.id.map);

			map = fm.getMap();

			if(map == null){

				//Toast.makeText(getActivity(), "null", 0).show();
			}
			else{

				map.setMyLocationEnabled(true);
				// checking for the best provider and connectivity

				// Getting LocationManager object from System Service LOCATION_SERVICE
				locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);

				if (netInfo !=null && netInfo.isConnected()) {


					if(isGpsEnable()){


						provider=LocationManager.GPS_PROVIDER;
						mLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

						if(mLocation==null){
							//onLocationChanged(location);

							provider=LocationManager.NETWORK_PROVIDER;
							mLocation = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

							Log.d("Location","Location " + mLocation + " " + provider);

							if(mLocation!=null){

								onLocationChanged(mLocation);
							}
							else{

								Toast.makeText(getActivity(), "Unable to fetch your current location at the moment. Please try again later.", Toast.LENGTH_LONG).show();
							}

						}else {
							onLocationChanged(mLocation);
						}

					}
					else{

						showGpsAlert();
					}

				}
				else{

					Toast.makeText(getActivity(), "No internet connection", Toast.LENGTH_LONG).show();
				}


				//Toast.makeText(getActivity(), "Not null", 0).show();

				// Enabling MyLocation Layer of Google Map
				//map.setMyLocationEnabled(true);

				// Getting LocationManager object from System Service LOCATION_SERVICE
				//				locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
				//
				//
				//				mLocation = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
				//
				//				if(mLocation!=null){
				//					onLocationChanged(mLocation);
				//				}
				//locationManager.requestLocationUpdates(provider, 20000, 0, this);
			}
		}

		getPref();

		mTvPark.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub




				if(mSaved_lati!=0 && mSaved_longi!=0){

					AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
					alertDialogBuilder.setTitle("Nissan QRG");
					alertDialogBuilder.setIcon(R.drawable.ic_launcher);
					alertDialogBuilder.setMessage("Previously parked location will be erased. You want to continue?")
					.setPositiveButton("Yes",new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,int id) {
							// if this button is clicked, close
							// current activity

							Log.d("Position ","Latitude: " + mLatitude);
							Log.d("Position ","Longitude: " + mLongitude);
							if(mLatitude ==0 || mLongitude == 0){

								Toast.makeText(getActivity(), "Unable to fetch your current location,  please check Google location service and restart your phone.",Toast.LENGTH_LONG).show();
							}
							else{
								map.clear();
								savePref();
							}

						}
					})
					.setNegativeButton("No",new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,int id) {
							// if this button is clicked, just close
							// the dialog box and do nothing

						}
					});

					Dialog alertDialog = alertDialogBuilder.create();
					alertDialog.setCanceledOnTouchOutside(false);
					alertDialog.show();


				}
				else{

					Log.d("Position ","Latitude: " + mLatitude);
					Log.d("Position ","Longitude: " + mLongitude);
					if(mLatitude ==0 || mLongitude == 0){

						Toast.makeText(getActivity(), "Cant find your current position,  please check google play service and restart your phone.",Toast.LENGTH_LONG).show();
					}
					else{
						map.clear();
						savePref();
					}

				}




			}
		});

		mTvFind.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				//getPref();

				Log.d("Position ","Latitude: " + mLatitude);
				Log.d("Position ","Longitude: " + mLongitude);

				Log.d("Position ","Latitude saved: " + mSaved_lati);
				Log.d("Position ","Longitude saved: " + mSaved_longi);

				if(mSaved_lati==0 || mSaved_longi == 0){

					Toast.makeText(getActivity(), "Car not Parked", 0).show();
				}
				else{

					String uri = "http://maps.google.com/maps?saddr=" + mLatitude +","+ mLongitude +"&daddr="+ mSaved_lati +","+ mSaved_longi;


					Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(uri));
					intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
					startActivity(intent);
					//Toast.makeText(getActivity(), "Car Parked", 0).show();
				}
			}
		});




		return view;
	}




	@Override
	public void onDestroyView() {
		super.onDestroyView();
		try{
			SupportMapFragment fragment = ((SupportMapFragment) getFragmentManager().findFragmentById(R.id.map));
			FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
			ft.remove(fragment);
			ft.commit();
		}catch(Exception e){
		}

	}


	@Override
	public void onDestroy() {
		super.onDestroy();
	}

	@Override
	public void onResume() {
		super.onResume();
		if (map == null) {
			map = fm.getMap();

		}
	}


	private void initViews(View view){


		mTvPark = (TextView) view.findViewById(R.id.tvFindCarPark);
		mTvFind = (TextView) view.findViewById(R.id.tvFindCar);
		mEtParkHint = (EditText) view.findViewById(R.id.etParkHint);
	}


	@Override
	public void onLocationChanged(Location location) {
		// TODO Auto-generated method stub




		// Getting latitude of the current location
		mLatitude = location.getLatitude();

		// Getting longitude of the current location
		mLongitude = location.getLongitude();


		Log.d("Position ","Latitude: " + mLatitude);
		Log.d("Position ","Longitude: " + mLongitude);

		// Creating a LatLng object for the current location
		LatLng latLng = new LatLng(mLatitude, mLongitude);

		// Showing the current location in Google Map
		map.moveCamera(CameraUpdateFactory.newLatLng(latLng));

		// Zoom in the Google Map
		map.animateCamera(CameraUpdateFactory.zoomTo(13));



	}


	@Override
	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub

	}


	@Override
	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub

	}


	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub

	}


	void savePref(){

		Toast.makeText(getActivity(), "Your car has been parked", 0).show();

		mSaved_lati = mLatitude;
		mSaved_longi = mLongitude;

		Log.d("SAVED","SAVED " + mSaved_lati);

		mPrefs = getActivity().getSharedPreferences(SHARED_PREFERENCES_NAME, getActivity().MODE_WORLD_READABLE);
		mEditor = mPrefs.edit();


		mEditor.putString("Notes", mEtParkHint.getText().toString());
		mEditor.putFloat(LATITUDE, (float) mLatitude);
		mEditor.putFloat(LONGITUDE, (float) mLongitude);
		mEditor.commit();

		Marker carPos = map.addMarker(new MarkerOptions()
		.position(new LatLng(mSaved_lati, mSaved_longi))
		.icon(BitmapDescriptorFactory.fromResource(R.drawable.car)));

	}


	void getPref(){
		SharedPreferences shrd = getActivity().getSharedPreferences(SHARED_PREFERENCES_NAME, getActivity().MODE_WORLD_READABLE);
		mSaved_lati = (double)shrd.getFloat(LATITUDE, 0);
		mSaved_longi = (double)shrd.getFloat(LONGITUDE, 0);

		mEtParkHint.setText(shrd.getString("Notes", ""));

		//Toast.makeText(getActivity(), " " + shrd.getString("Notes", "") , 0).show();

		if(mSaved_lati!=0 && mSaved_longi!=0){

			Marker carPos = map.addMarker(new MarkerOptions()
			.position(new LatLng(mSaved_lati, mSaved_longi))
			.icon(BitmapDescriptorFactory.fromResource(R.drawable.car)));


		}

	}

	public boolean isGpsEnable(){
		boolean isGpsOn = false;
		try{
			LocationManager locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
			isGpsOn = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
		}catch(Exception ex){
			ex.printStackTrace();
		}
		return isGpsOn;
	}


	// Get Location Manager and check for GPS & Network location services

	public void showGpsAlert(){

		// Build the alert dialog
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle("Location Services Not Active");
		builder.setMessage("Please enable Location Services and GPS Satellites for better Results");
		builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialogInterface, int i) {
				// Show location settings when the user acknowledges the alert dialog
				//					Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
				//					startActivityForResult(intent, 10);
			}
		});
		builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				//if user doesnt turn on location services put location found as false
				//					try
				//					{
				//
				//						dialog.dismiss();
				//					}catch(Exception ex)
				//					{
				//						ex.printStackTrace();
				//					}
			}
		});
		Dialog alertDialog = builder.create();
		alertDialog.setCanceledOnTouchOutside(false);
		alertDialog.show();


	}

}
