package com.fragments;


import android.app.Dialog;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.adapters.AdapterData;
import com.adapters.GridViewAdapter;
import com.adapters.TipsContactListViewAdapter;
import com.phonethics.nissanqrg.R;

public class TrafficRules extends Fragment {

	private GridView mGvTrafficRules;
	
	private AdapterData mAdapterData;
	
	private String[] mTrafficRules;
	
	private Dialog mdRuleInfo;
	
	private int miPosition;
	
	private Typeface mtfDialogFont;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.trafficrules, null);

		initViews(view);

		mAdapterData = new AdapterData();
		
		mTrafficRules = mAdapterData.trafficRulesText;

		mGvTrafficRules.setAdapter(new GridViewAdapter(getActivity(), 1));

		mGvTrafficRules.setOnItemClickListener(gridClick);

		return view;
	}


	@Override
	public void onDestroyView() {
		super.onDestroyView();
	}


	@Override
	public void onDestroy() {
		super.onDestroy();
	}


	private void initViews(View view){
		mGvTrafficRules = (GridView) view.findViewById(R.id.gvTrafficRules);
		mtfDialogFont = Typeface.createFromAsset(getActivity().getResources().getAssets(),"RobotoBold.ttf");
		
		mdRuleInfo = new Dialog(getActivity());
		mdRuleInfo.requestWindowFeature(Window.FEATURE_NO_TITLE);
	}


	private OnItemClickListener gridClick = new OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> adapterView, View view, int position,
				long arg3) {
			
			miPosition = position;
			showDialog();
			
		}
	};
	
	private void showDialog(){
		
		mdRuleInfo.setContentView(R.layout.trafficrulesdialoglayout);

		
		WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
		Window window = mdRuleInfo.getWindow();
		lp.copyFrom(window.getAttributes());
		lp.width = WindowManager.LayoutParams.MATCH_PARENT;
		lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
		window.setAttributes(lp);
		
		final TextView tvDialog = (TextView) mdRuleInfo.findViewById(R.id.tvTrafficRule);
		final ImageView ivDialog = (ImageView) mdRuleInfo.findViewById(R.id.ivTrafficRule);
		ImageView ivNext = (ImageView) mdRuleInfo.findViewById(R.id.ivDialogNext);
		ImageView ivPrevious = (ImageView) mdRuleInfo.findViewById(R.id.ivDialogPrevious);
		Button btnOK = (Button) mdRuleInfo.findViewById(R.id.btnDialogOK);
		
		
		tvDialog.setText(mAdapterData.trafficRulesText[miPosition]);
		ivDialog.setImageResource(mAdapterData.trafficRulesIcon[miPosition]);
		tvDialog.setTypeface(mtfDialogFont);
		btnOK.setTypeface(mtfDialogFont);
		
		btnOK.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				mdRuleInfo.dismiss();
			}
		});
		
		ivNext.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				
				if(miPosition < mAdapterData.trafficRulesText.length-1){
					miPosition = miPosition + 1;

					tvDialog.setText(mAdapterData.trafficRulesText[miPosition]);
					ivDialog.setImageResource(mAdapterData.trafficRulesIcon[miPosition]);
				}
				else{
					Toast.makeText(getActivity(), "This is the last image", Toast.LENGTH_SHORT).show();
				}
			
			}
		});
		
		ivPrevious.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if(miPosition > 0){
					miPosition = miPosition - 1;

					tvDialog.setText(mAdapterData.trafficRulesText[miPosition]);
					ivDialog.setImageResource(mAdapterData.trafficRulesIcon[miPosition]);
				}
				else{
					Toast.makeText(getActivity(), "This is the first image", Toast.LENGTH_SHORT).show();
				}
			}
		});
		
		mdRuleInfo.show();
	}

}
