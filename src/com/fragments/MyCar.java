package com.fragments;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.util.ArrayList;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import com.log.NissanLog;
import com.phonethics.nissanqrg.R;
import com.utils.CameraImageSave;
import com.utils.DBUtils;

public class MyCar extends Fragment {


	private ArrayList<String> meditCarDetails;
	private ArrayList<String> msavedCarDetails;

	private EditText mEtCarBrand, mEtCarModel, mEtRegNumber;
	private EditText mEtPurchaseDate, mEtMfgDate, mEtRcNo; 
	private EditText mEtInsurerName, mEtPolicyNo, mEtPolicyDate; 
	private EditText mEtChasisNo;

	private Button mbtnSaveDetails;
	private Button mbtnCancel;
	private Button mBtnEditProfile;

	private ImageView mIvMyCar;
	private ImageView mIvSelectImage;
	private ImageView mIvMyCarBig;

	private TextView mTvMyCarName;

	private ViewFlipper mViewFlipper; 

	private DBUtils mdbUtils;

	private Dialog mdShowPictureOptions;

	private String[] mTableColumn = {
			"id",
			"CarBrand", "CarModel", "RegNo", "PurchaseData",
			"ManufactureDate", "RcNumber", "InsurerName",
			"PolicyNumber", "PolicyDate", "ChasisNumber"
	};

	private final int REQUESTCODE_CAMERA_CLICK = 1;
	private final int REQUESTCODE_GALLERY_SELECT = 2;
	private final int IMAGE_SIZE = 500;

	private boolean mbIsEditProfile = true;

	private CameraImageSave mCameraOperations;

	private WeakReference<Bitmap> mBitmap; 

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.mycar, null);

		initViews(view);
		meditCarDetails = new ArrayList<String>();
		msavedCarDetails = new ArrayList<String>();
		mdbUtils = new DBUtils(getActivity());
		mCameraOperations = new CameraImageSave();

		loadImage();

		mdbUtils.createTable("MyCar", mTableColumn);
		msavedCarDetails = mdbUtils.getData("MyCar", mTableColumn);
		if(msavedCarDetails.size() > 0)
			mTvMyCarName.setText(msavedCarDetails.get(1));
		if(msavedCarDetails.size() > 0){
			setCarData();
		}

		Typeface type = Typeface.createFromAsset(getActivity().getResources().getAssets(),"Multicolore.otf"); 
		mTvMyCarName.setTypeface(type);
		mBtnEditProfile.setTypeface(type);

		mViewFlipper.setInAnimation(getActivity(), R.anim.viewflipperanimation);

		mBtnEditProfile.setOnClickListener(clickListener);
		mbtnSaveDetails.setOnClickListener(clickListener);
		mbtnCancel.setOnClickListener(clickListener);
		mIvSelectImage.setOnClickListener(clickListener);

		return view;
	}


	@Override
	public void onDestroyView() {
		super.onDestroyView();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
	}


	private void initViews(View view) {

		mViewFlipper = (ViewFlipper) view.findViewById(R.id.viewFlipperMyCar);

		mTvMyCarName = (TextView) view.findViewById(R.id.tvMyCarName);

		mIvMyCar = (ImageView) view.findViewById(R.id.ivMyCarCircle);
		mIvSelectImage = (ImageView) view.findViewById(R.id.ivMyCarSelectImage);
		mIvMyCarBig = (ImageView) view.findViewById(R.id.ivMyCarBig);

		meditCarDetails = new ArrayList<String>();

		mEtCarBrand = (EditText) view.findViewById(R.id.etCarBrand);
		mEtCarModel = (EditText) view.findViewById(R.id.etCarModel);
		mEtRegNumber = (EditText) view.findViewById(R.id.etRegNumber);
		mEtPurchaseDate = (EditText) view.findViewById(R.id.etPurchaseDate);
		mEtMfgDate = (EditText) view.findViewById(R.id.etMfgDate);
		mEtRcNo = (EditText) view.findViewById(R.id.etRcNo);
		mEtInsurerName = (EditText) view.findViewById(R.id.etInsurerName);
		mEtPolicyNo = (EditText) view.findViewById(R.id.etPolicyNo);
		mEtPolicyDate = (EditText) view.findViewById(R.id.etPolicyDate);
		mEtChasisNo = (EditText) view.findViewById(R.id.etChasisNo);

		mbtnSaveDetails = (Button) view.findViewById(R.id.btnEditDone);
		mbtnCancel = (Button) view.findViewById(R.id.btnEditCancel);
		mBtnEditProfile = (Button) view.findViewById(R.id.btnEditProfile);

		mdShowPictureOptions = new Dialog(getActivity());
		mdShowPictureOptions.requestWindowFeature(Window.FEATURE_NO_TITLE);
	}


	/**
	 * Crops an image bitmap into a circle
	 * @param bitmap
	 * @return circluar bitmap
	 * @author Anirudh
	 */
	public static Bitmap getCircularBitmap(Bitmap bitmap) {
		Bitmap output;

		if (bitmap.getWidth() > bitmap.getHeight()) {
			output = Bitmap.createBitmap(bitmap.getHeight(), bitmap.getHeight(), Config.ARGB_8888);
		} else {
			output = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getWidth(), Config.ARGB_8888);
		}

		Canvas canvas = new Canvas(output);
		final int color = 0xff424242;
		final Paint paint = new Paint();
		final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());

		float r = 0;

		if (bitmap.getWidth() > bitmap.getHeight()) {
			r = bitmap.getHeight() / 2;
		} else {
			r = bitmap.getWidth() / 2;
		}

		paint.setAntiAlias(true);
		canvas.drawARGB(0, 0, 0, 0);
		paint.setColor(color);
		canvas.drawCircle(r, r, r, paint);
		paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
		canvas.drawBitmap(bitmap, rect, rect, paint);

		output = Bitmap.createScaledBitmap(output, 150, 150, true);
		return output;
	}


	//Down scaling bitmap to get Thumbnail
	public  Bitmap getThumbnail(Uri uri,int THUMBNAIL) throws FileNotFoundException, IOException{
		
		InputStream input = getActivity().getContentResolver().openInputStream(uri);

		BitmapFactory.Options onlyBoundsOptions = new BitmapFactory.Options();
		onlyBoundsOptions.inJustDecodeBounds = true;
		onlyBoundsOptions.inDither=true;
		onlyBoundsOptions.inPreferredConfig=Bitmap.Config.RGB_565;
		BitmapFactory.decodeStream(input, null, onlyBoundsOptions);
		input.close();

		if ((onlyBoundsOptions.outWidth == -1) || (onlyBoundsOptions.outHeight == -1))
			return null;

		int originalSize = (onlyBoundsOptions.outHeight > onlyBoundsOptions.outWidth) ? onlyBoundsOptions.outHeight : onlyBoundsOptions.outWidth;

		double ratio = (originalSize > THUMBNAIL) ? (originalSize / THUMBNAIL) : 1.0;

		BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
		bitmapOptions.inSampleSize = getPowerOfTwoForSampleRatio(ratio);
		bitmapOptions.inDither=true;
		bitmapOptions.inPreferredConfig=Bitmap.Config.RGB_565;

		input = getActivity().getContentResolver().openInputStream(uri);
		Bitmap bitmap = BitmapFactory.decodeStream(input, null, bitmapOptions);

		input.close();
		return bitmap;
	}

	private static int getPowerOfTwoForSampleRatio(double ratio){
		int k = Integer.highestOneBit((int)Math.floor(ratio));
		if(k==0) return 2;
		else return k;
	}


	private void getCarData() {
		if(meditCarDetails.size() > 0){
			meditCarDetails.clear();
		}

		meditCarDetails.add("1");
		meditCarDetails.add(mEtCarBrand.getText().toString());
		meditCarDetails.add(mEtCarModel.getText().toString());
		meditCarDetails.add(mEtRegNumber.getText().toString());
		meditCarDetails.add(mEtPurchaseDate.getText().toString());
		meditCarDetails.add(mEtMfgDate.getText().toString());
		meditCarDetails.add(mEtRcNo.getText().toString());
		meditCarDetails.add(mEtInsurerName.getText().toString());
		meditCarDetails.add(mEtPolicyNo.getText().toString());
		meditCarDetails.add(mEtPolicyDate.getText().toString());
		meditCarDetails.add(mEtChasisNo.getText().toString());
	}


	private void setCarData() {

		mEtCarBrand.setText(msavedCarDetails.get(1));
		mEtCarModel.setText(msavedCarDetails.get(2));
		mEtRegNumber.setText(msavedCarDetails.get(3));
		mEtPurchaseDate.setText(msavedCarDetails.get(4));
		mEtMfgDate.setText(msavedCarDetails.get(5));
		mEtRcNo.setText(msavedCarDetails.get(6));
		mEtInsurerName.setText(msavedCarDetails.get(7));
		mEtPolicyNo.setText(msavedCarDetails.get(8));
		mEtPolicyDate.setText(msavedCarDetails.get(9));
		mEtChasisNo.setText(msavedCarDetails.get(10));
	}


	private void showDialog(){

		mdShowPictureOptions.setContentView(R.layout.takepicturedialoglayout);

		WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
		Window window = mdShowPictureOptions.getWindow();
		lp.copyFrom(window.getAttributes());
		lp.width = WindowManager.LayoutParams.MATCH_PARENT;
		lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
		window.setAttributes(lp);

		final TextView tvTitle = (TextView) mdShowPictureOptions.findViewById(R.id.tvDialogTitle);
		Button btnCamera = (Button) mdShowPictureOptions.findViewById(R.id.btnCamera);
		Button btnGallery = (Button) mdShowPictureOptions.findViewById(R.id.btnGallery);

		Typeface type = Typeface.createFromAsset(getActivity().getResources().getAssets(),"RobotoBold.ttf"); 
		tvTitle.setTypeface(type);
		btnCamera.setTypeface(type);
		btnGallery.setTypeface(type);

		btnCamera.setOnClickListener(clickListener);
		btnGallery.setOnClickListener(clickListener);

		mdShowPictureOptions.show();
	}


	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if(resultCode == Activity.RESULT_OK){

			if(requestCode == REQUESTCODE_GALLERY_SELECT){
				if(data!=null){
					Uri imageUri = data.getData();

					if(imageUri != null){
						try{
							mdShowPictureOptions.dismiss();

							mBitmap = new WeakReference<Bitmap>(getThumbnail(imageUri, IMAGE_SIZE));
							mIvMyCar.setImageBitmap(getCircularBitmap(mBitmap.get()));
						} catch (FileNotFoundException e1){
							NissanLog.nissanLogE("MyCar", "File not found exception");
							e1.printStackTrace();
						} catch (IOException e2) {
							NissanLog.nissanLogE("MyCar", "IO exception exception");
							e2.printStackTrace();
						} catch (Exception e) {
							NissanLog.nissanLogE("MyCar", "exception");
							e.printStackTrace();
						}
					}
					else
						showToast("There was a problem in processing that image. Please try again");

				} 
				else 
					showToast("There was a problem in processing that image. Please try again");

			} 

			else if(requestCode == REQUESTCODE_CAMERA_CLICK){
				if(data != null){
					
					mBitmap = new WeakReference<Bitmap>((Bitmap) data.getExtras().get("data"));
					
					if(mBitmap.get() != null){
						try{
							mdShowPictureOptions.dismiss();

							mBitmap = new WeakReference<Bitmap>
								(getThumbnail(mCameraOperations.getImageUri(getActivity(), mBitmap.get())
									, IMAGE_SIZE));
							mIvMyCar.setImageBitmap(getCircularBitmap(mBitmap.get()));
						} catch (FileNotFoundException e1){
							NissanLog.nissanLogE("MyCar", "File not found exception");
							e1.printStackTrace();
						} catch (IOException e2) {
							NissanLog.nissanLogE("MyCar", "IO exception exception");
							e2.printStackTrace();
						} catch (Exception e) {
							NissanLog.nissanLogE("MyCar", "exception");
							e.printStackTrace();
						}
					}
					else
						showToast("There was a problem in processing that image. Please try again");
					
					
				} else
					showToast("There was a problem in processing that image. Please try again");
			}

			mBtnEditProfile.setText("Save this image?");
			mBtnEditProfile.setBackgroundColor(Color.parseColor("#8BB381"));
			mbIsEditProfile = false;
		} 
		
		
		else {
			showToast("There was a problem in processing that image. Please try again");
		}

	};


	private void showToast (String message) {
		Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
	}
	
	
	private void loadImage(){
		WeakReference<Bitmap> bitmap = new WeakReference<Bitmap>
		(mCameraOperations.getBitmapFromFile());
		if(bitmap.get() != null) {
			mIvMyCarBig.setImageBitmap(bitmap.get());
			mIvMyCar.setImageBitmap(getCircularBitmap(bitmap.get()));
		} else {
			BitmapDrawable drawable = (BitmapDrawable) mIvMyCar.getDrawable();
			bitmap = new WeakReference<Bitmap>
			(getCircularBitmap(drawable.getBitmap()));
			mIvMyCar.setImageBitmap(bitmap.get());
		}
	}


	OnClickListener clickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.btnEditProfile: {
				if(mbIsEditProfile){
					loadImage();
					mViewFlipper.showNext();
				}
				else {
					mCameraOperations.saveBitmapToFile(mBitmap.get());
					mBtnEditProfile.setText("Edit Profile");
					mBtnEditProfile.setBackgroundColor(Color.parseColor("#006891"));
					mbIsEditProfile = true;
					showToast("Image saved!");
				}
				break;
			}

			case R.id.btnEditDone: {
				getCarData();
				mdbUtils.insertData(meditCarDetails, mTableColumn);
				mdbUtils.getData("MyCar", mTableColumn);
				Toast.makeText(getActivity(), "Details saved successfully", Toast.LENGTH_SHORT).show();
				mTvMyCarName.setText(mEtCarBrand.getText().toString());
				mViewFlipper.showPrevious();
				break;
			}

			case R.id.btnEditCancel: {
				mViewFlipper.showPrevious();
			}

			case R.id.ivMyCarSelectImage: {
				showDialog();
				break;
			}

			case R.id.btnGallery: { //Dialog
				Intent intent = new Intent(Intent.ACTION_PICK);
				intent.setType("image/*");
				startActivityForResult(intent, REQUESTCODE_GALLERY_SELECT);
				break;
			}

			case R.id.btnCamera: { //Dialog
				Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
				startActivityForResult(intent, REQUESTCODE_CAMERA_CLICK);
				break;
			}

			default:
				break;
			}
		}
	};
}
