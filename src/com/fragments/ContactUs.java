package com.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ProgressBar;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.adapters.TipsContactListViewAdapter;
import com.log.NissanLog;
import com.phonethics.nissanqrg.R;
import com.phonethics.nissanqrg.StoreList;
import com.phonethics.nissanqrg.WebViewActivity;

public class ContactUs extends Fragment {

	private ListView mLvContactUs;
	ProgressBar mProgressBar;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}


	/* Inflate the same view as Tips.java */
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.tips, null);

		initViews(view);

		mLvContactUs.setAdapter(new TipsContactListViewAdapter(getActivity(), 1,mProgressBar));

		mLvContactUs.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int pos,
					long arg3) {
				// TODO Auto-generated method stub


				if(pos == 1){

					// Book a test drive

					//Toast.makeText(getActivity(), "Book test drive", 0).show();
					AlertDialog.Builder alertDialog=new AlertDialog.Builder(getActivity());
					alertDialog.setIcon(R.drawable.ic_launcher);
					alertDialog.setTitle("Book Test Drive");
					alertDialog.setMessage("Do you want to call on this number?");
					alertDialog.setCancelable(true);
					alertDialog.setPositiveButton("Yes",new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub
							Intent call = new Intent(android.content.Intent.ACTION_DIAL);
							call.setData(Uri.parse(getResources().getString(R.string.car_customer)));
							startActivity(call);
							//overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
						}
					});	
					alertDialog.setNegativeButton("No",new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub

						}
					});

					AlertDialog alert=alertDialog.show();

				}
				else if(pos == 2){

					// Call Nissan Customer Care Support

					AlertDialog.Builder alertDialog=new AlertDialog.Builder(getActivity());
					alertDialog.setIcon(R.drawable.ic_launcher);
					alertDialog.setTitle("Nissan Customer Care");
					alertDialog.setMessage("Do you want to call on this number?");
					alertDialog.setCancelable(true);
					alertDialog.setPositiveButton("Yes",new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub
							Intent call = new Intent(android.content.Intent.ACTION_DIAL);
							call.setData(Uri.parse(getResources().getString(R.string.car_customer)));
							startActivity(call);
							//overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
						}
					});	
					alertDialog.setNegativeButton("No",new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub

						}
					});

					AlertDialog alert=alertDialog.show();

				}
				else if(pos == 3){

					// Call BreakDown Helpline

					AlertDialog.Builder alertDialog=new AlertDialog.Builder(getActivity());
					alertDialog.setIcon(R.drawable.ic_launcher);
					alertDialog.setTitle("BreakDown Helpline");
					alertDialog.setMessage("Do you want to call on this number?");
					alertDialog.setCancelable(true);
					alertDialog.setPositiveButton("Yes",new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub
							Intent call = new Intent(android.content.Intent.ACTION_DIAL);
							call.setData(Uri.parse(getResources().getString(R.string.breakdown_helpline)));
							startActivity(call);
							//overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
						}
					});	
					alertDialog.setNegativeButton("No",new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub

						}
					});

					AlertDialog alert=alertDialog.show();

				}
				else if(pos == 5){

					// Email Customer Support

					Intent email=new Intent(Intent.ACTION_SEND);
					email.putExtra("android.intent.extra.EMAIL",new String[]{getResources().getString(R.string.emailId)});
					email.putExtra("android.intent.extra.SUBJECT", "Inquiry for NISSAN" + " ");
					email.setType("message/rfc822");
					startActivity(Intent.createChooser(email, "Send mail..."));
					//overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

				}
				else if(pos == 6){

					// Email Breakdown Help


				}
				else if(pos == 8){

					// Facebook Connect
					Intent intent=new Intent(getActivity(), WebViewActivity.class);
					startActivity(intent);
					//overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

				}
				else if(pos == 9){

					// Dealor Locator
					Intent intent = new Intent(getActivity(),StoreList.class);
					startActivity(intent);
				}
			}
		});

		return view;
	}


	@Override
	public void onDestroyView() {
		NissanLog.nissanLogI("Contact us", "Fragment lifecycle destroy view");
		super.onDestroyView();
	}


	@Override
	public void onDestroy() {
		NissanLog.nissanLogI("Contact us", "Fragment lifecycle destroy");
		super.onDestroy();
	}


	private void initViews(View view){
		mLvContactUs = (ListView) view.findViewById(R.id.lvTips);
		mProgressBar = (ProgressBar) view.findViewById(R.id.progressBar);
	}
}
