
package com.fragments;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.SQLException;
import android.graphics.Typeface;
import android.net.ParseException;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;
import android.widget.ViewFlipper;

import com.database.DatabaseUtils;
import com.log.NissanLog;
import com.phonethics.nissanqrg.AlarmReciever;
import com.phonethics.nissanqrg.R;



public class Reminder extends Fragment {

	private TextView mTvAdd;
	private TextView mTvPending;
	private TextView mTvComplete;

	/* Button click for reminder main */
	private LinearLayout mAdd;
	private LinearLayout mLayoutReminderMain;
	private LinearLayout mLayoutReminderAdd;
	private LinearLayout mLayoutReminderPending;
	private LinearLayout mLayoutReminderComplete;

	private EditText mReminderTime;
	private EditText mReminderDate;
	private EditText mReminderNotes;



	private ViewFlipper mViewFlipper;
	private ListView mReminderList;

	private Button mBtnReminderSave;
	private Button mBtnReminderCancel;


	private DatabaseUtils mdbUtils;

	ReminderListAdapter reminderListAdapter;

	//Calender variables
	int date;
	int month;
	int year;

	int hour;
	int minutes;
	int seconds;

	// To check for the time difference
	Calendar tempCal = Calendar.getInstance();

	Calendar cal1 = Calendar.getInstance();

	SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");

	SimpleDateFormat formatterTime = new SimpleDateFormat("HH:mm");

	int result;
	int reminderId;

	Calendar dateTime=Calendar.getInstance();
	Calendar dateTimeCurrent=Calendar.getInstance();

	int mReminderId;
	boolean mIsCompleted = false;

	ArrayList<String> reminderIdsList = new ArrayList<String>();
	ArrayList<String> alarmMessage = new ArrayList<String>();
	ArrayList<String> alarmDate = new ArrayList<String>();
	ArrayList<String> alarmTime = new ArrayList<String>();

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);


	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.reminder, null);

		initViews(view);

		//Creating Table

		mdbUtils = new DatabaseUtils(getActivity());



		Typeface type = Typeface.createFromAsset(getActivity().getResources().getAssets(),"Multicolore.otf"); 
		mTvAdd.setTypeface(type);
		mTvPending.setTypeface(type);
		mTvComplete.setTypeface(type);

		mViewFlipper.setInAnimation(getActivity(), R.anim.viewflipperanimation);


		/* Click listener */
		mAdd.setOnClickListener(clickListener);
		mReminderDate.setOnClickListener(clickListener); 
		mReminderTime.setOnClickListener(clickListener); 

		mBtnReminderCancel.setOnClickListener(clickListener);
		mBtnReminderSave.setOnClickListener(clickListener);
		mLayoutReminderPending.setOnClickListener(clickListener);
		mLayoutReminderComplete.setOnClickListener(clickListener);

		// Click Listner for Listview



		mReminderList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1,
					int position, long arg3) {
				// TODO Auto-generated method stub


				if(!mIsCompleted){


					//					NissanLog.nissanLogV("Display ","Info " + alarmMessage.get(position));
					//					NissanLog.nissanLogV("Display ","Info " + alarmDate.get(position));
					//					NissanLog.nissanLogV("Display ","Info " + alarmTime.get(position));

					final Dialog dialog = new Dialog(getActivity());
					dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
					dialog.setContentView(	R.layout.alarmdetail);


					TextView alarmNoteText = (TextView)  dialog.findViewById(R.id.alarmNoteText);
					TextView alarmDateText = (TextView)  dialog.findViewById(R.id.alarmDateText);
					TextView alarmTimeText = (TextView)  dialog.findViewById(R.id.alarmTimeText);

					alarmNoteText.setText(alarmMessage.get(position));
					alarmDateText.setText(alarmDate.get(position));
					alarmTimeText.setText(alarmTime.get(position));

					Button dialogButton = (Button) dialog.findViewById(R.id.btnDialogOK);
					// if button is clicked, close the custom dialog
					dialogButton.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View v) {
							dialog.dismiss();
						}
					});

					dialog.show();

				}


			}
		});

		mReminderList.setOnItemLongClickListener(new OnItemLongClickListener() {


			public boolean onItemLongClick(AdapterView<?> arg0, View v,
					final int position, long arg3) {


				if(mIsCompleted){



					//Toast.makeText(getActivity(), "Inside completed " + position, 0).show();

					AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());

					// set titl
					alertDialogBuilder.setTitle("Reminder");

					// set dialog message
					alertDialogBuilder
					.setMessage("Are you sure you want to delete this reminder?")
					.setCancelable(false)
					.setPositiveButton("Yes",new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,int id) {
							// if this button is clicked, close
							// current activity

							try {

								mdbUtils.open();
								mdbUtils.deleteReminder(Integer.parseInt(reminderIdsList.get(position)));
								mdbUtils.close();

							} catch (Exception e) {
								// TODO: handle exception
								e.printStackTrace();
							}

							//							Toast.makeText(getActivity(), "Selected Reminder has been deleted", Toast.LENGTH_SHORT).show();
							//							mViewFlipper.setVisibility(View.VISIBLE);
							//							mReminderList.setVisibility(View.GONE);

							reminderIdsList.clear();
							callCompletedReminders();
						}
					})
					.setNegativeButton("No",new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,int id) {
							// if this button is clicked, just close
							// the dialog box and do nothing


						}
					});

					// create alert dialog
					AlertDialog alertDialog = alertDialogBuilder.create();

					// show it
					alertDialog.show();

				}
				return true;
			}
		});

		return view;
	}


	@Override
	public void onDestroyView() {
		super.onDestroyView();
	}


	@Override
	public void onDestroy() {
		super.onDestroy();
	}


	private void initViews(View view){
		mTvAdd = (TextView) view.findViewById(R.id.tvAdd);
		mTvPending = (TextView) view.findViewById(R.id.tvPending);
		mTvComplete = (TextView) view.findViewById(R.id.tvComplete);

		mReminderList = (ListView) view.findViewById(R.id.alarmList);
		mLayoutReminderAdd = (LinearLayout) view.findViewById(R.id.layoutReminderAdd);
		mLayoutReminderMain = (LinearLayout) view.findViewById(R.id.layoutReminderMain);
		mLayoutReminderPending = (LinearLayout) view.findViewById(R.id.layoutPendingReminder);
		mLayoutReminderComplete = (LinearLayout) view.findViewById(R.id.layoutCompleteReminder);

		mAdd = (LinearLayout) view.findViewById(R.id.layoutAddReminder);

		mReminderDate = (EditText) view.findViewById(R.id.etDate);
		mReminderTime = (EditText) view.findViewById(R.id.etTime);

		mViewFlipper = (ViewFlipper) view.findViewById(R.id.viewFlipperReminder);

		mReminderNotes = (EditText) view.findViewById(R.id.etAddNote);

		mBtnReminderSave = (Button) view.findViewById(R.id.btnReminderSave);
		mBtnReminderCancel = (Button) view.findViewById(R.id.btnReminderCancel);
	}


	OnClickListener clickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			/* Show layout for add reminder */
			case R.id.layoutAddReminder:{
				mViewFlipper.showNext();
				break;
			}

			/* Show dialog for add date */
			case R.id.etDate:{

				//				final Calendar c = Calendar.getInstance();
				//				int mYear = c.get(Calendar.YEAR);
				//				int mMonth = c.get(Calendar.MONTH);
				//				int mDay = c.get(Calendar.DAY_OF_MONTH);
				//
				//				DatePickerDialog dpd = new DatePickerDialog(getActivity(),
				//						new DatePickerDialog.OnDateSetListener() {
				//
				//					@Override
				//					public void onDateSet(DatePicker view, int year,
				//							int monthOfYear, int dayOfMonth) {
				//						mReminderDate.setText(dayOfMonth + "-"
				//								+ (monthOfYear + 1) + "-" + year);
				//					}
				//				}, mYear, mMonth, mDay);
				//				dpd.show();

				chooseDate();

				break;
			}

			/* Show dialog for add time */
			case R.id.etTime:{
				//				final Calendar c = Calendar.getInstance();
				//				int mHour = c.get(Calendar.HOUR);
				//				int mMinute = c.get(Calendar.MINUTE);
				//
				//				TimePickerDialog tpd = new TimePickerDialog(getActivity(),
				//						new TimePickerDialog.OnTimeSetListener() {
				//
				//					@Override
				//					public void onTimeSet(TimePicker view, int hourOfDay,
				//							int minute) {
				//						mReminderTime.setText(hourOfDay + ":" + minute);
				//					}
				//				}, mHour, mMinute, false);
				//				tpd.show();

				chooseTime();
				break;
			}

			// Save or Set Reminder
			case R.id.btnReminderSave:{


				if(mReminderDate.length()!=0 && mReminderTime.length()!=0 && mReminderNotes.length()!=0){

					addReminder();
					//	mViewFlipper.showPrevious();
					mViewFlipper.setVisibility(View.GONE);
					mReminderList.setVisibility(View.VISIBLE);

					ArrayList<ArrayList<Object>> reminder_details = new ArrayList<ArrayList<Object>>();
					try{
						mdbUtils.open();

						reminder_details = mdbUtils.getAllPendingReminders();


						NissanLog.nissanLogE("Size ","Size " + reminder_details.size());

						if(reminder_details.size() == 0){

							Toast.makeText(getActivity(), "No pending alarms", 0).show();
						}
						else{

							for(int i=0; i<reminder_details.size();i++){

								for(int k=0;k<reminder_details.get(i).size();k++){

									NissanLog.nissanLogV("Item ", "Item " +reminder_details.get(i).get(0).toString());

								}
								alarmMessage.add(reminder_details.get(i).get(2).toString());
								alarmDate.add(reminder_details.get(i).get(3).toString());
								alarmTime.add(reminder_details.get(i).get(4).toString());

							}

							for(int i=0;i<reminderIdsList.size();i++){

								NissanLog.nissanLogE("ITEMS IDS","ITEMS IDS " + reminderIdsList.get(i));
							}

							reminderListAdapter = new ReminderListAdapter(getActivity(),0, reminder_details);
							mReminderList.setAdapter(reminderListAdapter);

						}


						mdbUtils.close();


					}catch(SQLException ex){
						ex.printStackTrace();
					}finally{
						mdbUtils.close();
					}

				}
				else{

					Toast.makeText(getActivity(), "Please fill given details", Toast.LENGTH_SHORT).show();
				}

				break;
			}

			// Cancel Adding Reminders
			case R.id.btnReminderCancel:{

				mViewFlipper.showPrevious();
				break;
			}

			// Get Pending Reminders
			case R.id.layoutPendingReminder:{

				mViewFlipper.setVisibility(View.GONE);
				mReminderList.setVisibility(View.VISIBLE);

				ArrayList<ArrayList<Object>> reminder_details = new ArrayList<ArrayList<Object>>();
				try{
					mdbUtils.open();

					reminder_details = mdbUtils.getAllPendingReminders();

					NissanLog.nissanLogE("Size ","Size " + reminder_details.size());

					if(reminder_details.size() == 0){

						Toast.makeText(getActivity(), "No pending alarms", 0).show();
					}
					else{

						for(int i=0; i<reminder_details.size();i++){

							for(int k=0;k<reminder_details.get(i).size();k++){

								NissanLog.nissanLogV("Item ", "Item " +reminder_details.get(i).get(k));

							}

							alarmMessage.add(reminder_details.get(i).get(2).toString());
							alarmDate.add(reminder_details.get(i).get(3).toString());
							alarmTime.add(reminder_details.get(i).get(4).toString());
						}

						reminderListAdapter = new ReminderListAdapter(getActivity(),0, reminder_details);
						mReminderList.setAdapter(reminderListAdapter);

					}


					mdbUtils.close();


				}catch(SQLException ex){
					ex.printStackTrace();
				}finally{
					mdbUtils.close();
				}

				break;
			}

			// Get Completed Reminders
			case R.id.layoutCompleteReminder:{

				callCompletedReminders();
				break;
			}


			default:
				//Toast.makeText(getActivity(), "Coming soon", Toast.LENGTH_SHORT).show();
				break;
			}
		}



	};


	// for creating reminders in DB

	private void addReminder() {
		// TODO Auto-generated method stub

		try{
			mdbUtils.open();
			mdbUtils.addReminder(mReminderNotes.getText().toString(), mReminderDate.getText().toString(), mReminderTime.getText().toString(),"","");
			mdbUtils.close();

		}catch(SQLException sqlex){
			NissanLog.nissanLogE("ADD SQLERROR",sqlex.toString());
		}catch(Exception ex){
			NissanLog.nissanLogE("ADD Exception",ex.toString());
		}
	}


	// setting list view for pending reminders


	class ReminderListAdapter extends ArrayAdapter<String>{

		LayoutInflater layoutInflater;
		ArrayList<ArrayList<Object>> reminder_details = new ArrayList<ArrayList<Object>>();

		public ReminderListAdapter(Context context, int resource, 	ArrayList<ArrayList<Object>> reminder_details) {
			super(context, resource);
			// TODO Auto-generated constructor stub

			this.reminder_details = reminder_details;
			layoutInflater=getActivity().getLayoutInflater();
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return reminder_details.size();
		}


		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub

			//View rowView = convertView;



			if (convertView == null) {

				convertView = layoutInflater.inflate(R.layout.reminderlist,parent, false);

				ReminderViewHolder holder = new ReminderViewHolder();

				holder.reminderNotes = (TextView) convertView.findViewById(R.id.reminderNotes);

				try {

					Date dateObj, timeObj;

					try {

						mReminderId =  Integer.parseInt(reminder_details.get(position).get(0).toString().trim());

						String tmpTime = reminder_details.get(position).get(4).toString().trim();

						String tmpHour = tmpTime.substring(0, tmpTime.indexOf(":")).trim();

						String tmpMin =	tmpTime.substring(3, 5);

						Log.d("NEWTIME","NEWTIME " + tmpHour);
						Log.d("NEWTIME","NEWTIME " + tmpMin);

						hour			= Integer.parseInt(tmpHour);
						minutes				= Integer.parseInt(tmpMin);

						dateObj = formatter.parse((reminder_details.get(position).get(3).toString()));

						timeObj = formatterTime.parse((reminder_details.get(position).get(4).toString()));

						Calendar cal = Calendar.getInstance();
						cal.setTime(dateObj);

						date = cal.get(Calendar.DATE);
						month = cal.get(Calendar.MONTH);
						year = cal.get(Calendar.YEAR);

						cal.setTime(timeObj);

						Log.d("DATES","DATES "  +  timeObj.toString());

						//						hour = cal.get(Calendar.HOUR);
						//						minutes = cal.get(Calendar.MINUTE);
						//						seconds = cal.get(Calendar.SECOND);

						//int HOUR = 


						// tempCal -- current date
						// cal1 -- alarm time
						cal1.set(year, month, date, hour, minutes,0);
						cal.set(year, month, date, hour, minutes,0);

						Date d = new Date(cal1.getTimeInMillis());

						Log.d("DATES","DATES "  +  d.toString());

						Log.d("Time","Time tmp:" + tempCal.get(Calendar.DATE)+" Min  "+tempCal.get(Calendar.HOUR));
						Log.d("Time","Time usr:" + cal1.get(Calendar.DATE)+" Min  "+cal1.get(Calendar.HOUR));
						Log.d("Time","Time cal:" + cal.get(Calendar.DATE)+" Min  "+cal.get(Calendar.HOUR));

						result = tempCal.compareTo(cal1);
						NissanLog.nissanLogV("Date ","Date " +result);


					} catch (java.text.ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}


				} catch (ParseException e) {
					e.printStackTrace();
				}


				NissanLog.nissanLogV("YEAR ", "Cal YEAR " + cal1.get(Calendar.YEAR));
				NissanLog.nissanLogV("CAL! ", "Cal Date " + cal1.get(Calendar.DATE));
				NissanLog.nissanLogV("CAL1 ", "Cal Month " + cal1.get(Calendar.MONTH));
				Calendar presentDate = Calendar.getInstance();
				if((presentDate.compareTo(cal1))<0){



					//Long time = new GregorianCalendar().getTimeInMillis()+24*60*60*1000;

					reminderId = reminderId + 1;

					//Toast.makeText(getActivity(), "Alarm inside if", 0).show();
					Intent intentAlarm = new Intent(getActivity(), AlarmReciever.class);
					intentAlarm.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
					intentAlarm.putExtra("note",reminder_details.get(position).get(2).toString());
					intentAlarm.putExtra("reminderId",mReminderId);

					// create the object
					AlarmManager alarmManager = (AlarmManager) getActivity().getSystemService(Context.ALARM_SERVICE);

					//set the alarm for particular time

					alarmManager.set(AlarmManager.RTC_WAKEUP,cal1.getTimeInMillis(), PendingIntent.getBroadcast(getActivity(),reminderId,  intentAlarm, PendingIntent.FLAG_UPDATE_CURRENT));

					//Toast.makeText(this, "Alarm Scheduled for Tommrrow", Toast.LENGTH_LONG).show();

				}
				else{

					//Toast.makeText(getActivity(), "inside else", 0).show();
					mdbUtils.open();
					mdbUtils.setAlarm(mReminderId,"1");
					mdbUtils.close();
				}



				convertView.setTag(holder);
			}


			ReminderViewHolder hold = (ReminderViewHolder) convertView.getTag();
			hold.reminderNotes.setText(reminder_details.get(position).get(2).toString());





			return convertView;

		}



	}

	class ReminderViewHolder {

		TextView reminderNotes;
	}


	public void chooseDate(){


		dateTime.add(Calendar.DAY_OF_MONTH, 0);
		new DatePickerDialog(getActivity(), d, dateTime.get(Calendar.YEAR),dateTime.get(Calendar.MONTH),dateTime.get(Calendar.DAY_OF_MONTH)).show();
		DatePickerDialog.OnDateSetListener d=new DatePickerDialog.OnDateSetListener() {
			@Override
			public void onDateSet(DatePicker view, int year, int monthOfYear,int dayOfMonth) {
				dateTime.set(Calendar.YEAR,year);
				dateTime.set(Calendar.MONTH, monthOfYear);
				dateTime.set(Calendar.DAY_OF_MONTH, dayOfMonth);
				//updateDate();
			}
		};


	}

	DatePickerDialog.OnDateSetListener d=new DatePickerDialog.OnDateSetListener() {
		@Override
		public void onDateSet(DatePicker view, int year, int monthOfYear,int dayOfMonth) {
			dateTime.set(Calendar.YEAR,year);
			dateTime.set(Calendar.MONTH, monthOfYear);
			dateTime.set(Calendar.DAY_OF_MONTH, dayOfMonth);
			updateDate();

		}




	};

	TimePickerDialog.OnTimeSetListener t=new TimePickerDialog.OnTimeSetListener() {
		@Override
		public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
			dateTime.set(Calendar.HOUR_OF_DAY, hourOfDay);
			dateTime.set(Calendar.MINUTE,minute);
			updateTime();
		}
	};

	public void chooseTime(){
		new TimePickerDialog(getActivity(), t, dateTime.get(Calendar.HOUR_OF_DAY), dateTime.get(Calendar.MINUTE), true).show();
	}


	private void updateTime() {

		DateFormat  dt2=new SimpleDateFormat(" HH:mm");
		//miliseconds=dateTime.getTimeInMillis();
		mReminderTime.setText(dt2.format(dateTime.getTime()));
	}

	private void updateDate() {
		// TODO Auto-generated method stub

		Date d1=dateTimeCurrent.getTime();
		Date d2=dateTime.getTime();


		if(dateDiff(d2, d1)<0){
			Toast.makeText(getActivity(), "Slected date cannot be less than current date", Toast.LENGTH_SHORT).show();
			//chooseDate();
		}
		else{

			DateFormat  dt=new SimpleDateFormat("yyyy-MM-dd");
			DateFormat  dtReview=new SimpleDateFormat("dd-MM-yyyy");
			mReminderDate.setText(dtReview.format(dateTime.getTime()));
		}

	}

	public long dateDiff(Date d1,Date d2)
	{
		long datediff=0;
		datediff=(d1.getTime()-d2.getTime())/(24 * 60 * 60 * 1000);
		Log.i("daysBetween", "daysBetween "+datediff);
		return datediff;
	}

	private void callCompletedReminders() {
		// TODO Auto-generated method stub





		//Toast.makeText(getActivity(), "Getting Completed Ones", Toast.LENGTH_SHORT).show();

		mIsCompleted = true;

		mViewFlipper.setVisibility(View.GONE);
		mReminderList.setVisibility(View.VISIBLE);


		ArrayList<ArrayList<Object>> reminder_details = new ArrayList<ArrayList<Object>>();


		try {

			mdbUtils.open();

			reminder_details = mdbUtils.getAllCompletedRemeinders();

			NissanLog.nissanLogE("Size ","Size " + reminder_details.size());

			if(reminder_details.size() == 0){

				Toast.makeText(getActivity(), "No completed alarms", 0).show();
				reminderListAdapter = new ReminderListAdapter(getActivity(),0, reminder_details);
				mReminderList.setAdapter(reminderListAdapter);
			}
			else{

				for(int i=0; i<reminder_details.size();i++){

					for(int k=0;k<reminder_details.get(i).size();k++){

						NissanLog.nissanLogV("Item ", "Item " +reminder_details.get(i).get(k));
						//reminderIdsList.add(reminder_details.get(i).get(0).toString());
					}

					reminderIdsList.add(reminder_details.get(i).get(0).toString());
				}

				reminderListAdapter = new ReminderListAdapter(getActivity(),0, reminder_details);
				mReminderList.setAdapter(reminderListAdapter);

			}


			mdbUtils.close();


		} catch (Exception e) {
			// TODO: handle exception

			e.printStackTrace();
		}



	}


}
