package com.phonethics.nissanqrg;

import com.fragments.Reminder;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager.LayoutParams;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class AlarmDialog extends Activity {

	Context context;
	TextView mNotes;
	String mdisplayNotes;

	Button mOk;
	Button mCancel;
	
	String packageName;
	ActivityManager activityManager;
	String baseActivity;
	String className;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(LayoutParams.FLAG_NOT_TOUCH_MODAL, LayoutParams.FLAG_NOT_TOUCH_MODAL);
		getWindow().setFlags(LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH, LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH);
		setContentView(R.layout.activity_alarm_dialog);

		context = this;

		mOk  = (Button) findViewById(R.id.button_ok);
		mCancel = (Button) findViewById(R.id.button_cancel);
		
		activityManager = (ActivityManager) context.getSystemService(Activity.ACTIVITY_SERVICE);
		packageName = activityManager.getRunningTasks(1).get(0).topActivity.getPackageName();
		className = activityManager.getRunningTasks(1).get(0).topActivity.getClassName();
		baseActivity = activityManager.getRunningTasks(1).get(0).baseActivity.getClassName();

		Bundle extras = getIntent().getExtras();

		if(extras!=null){

			mdisplayNotes = extras.getString("message");
		}

		Typeface type = Typeface.createFromAsset(context.getResources().getAssets(),"Multicolore.otf"); 

		mNotes = (TextView) findViewById(R.id.Notes);

		mNotes.setTypeface(type);

		mNotes.setText(mdisplayNotes);

		mOk.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				//Toast.makeText(context, "Clicked on Ok", 0).show();

				if(packageName.equals("com.phonethics.nissanqrg") && baseActivity.equals("com.phonethics.nissanqrg.AlarmDialog")){
					Intent intent = new Intent(context, LandingScreen.class);
					startActivity(intent);
					finish();
				}else if(packageName.equals("com.phonethics.nissanqrg") ){

					if(!className.equals("com.phonethics.nissanqrg.Reminder")){

						Intent intent = new Intent(context, LandingScreen.class);
						intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
						startActivity(intent);		
						finish();
					}
					else{

						finish();
					}

				}else{


					Intent intent = new Intent(context, LandingScreen.class);
					startActivity(intent);
					finish();

				}


			}
		});

		mCancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				finish();
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.alarm_dialog, menu);
		return true;
	}

}
