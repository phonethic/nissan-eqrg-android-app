package com.phonethics.nissanqrg;

import com.database.DatabaseUtils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

public class AlarmReciever extends BroadcastReceiver{

	String note="";
	int mRemId;
	
	private DatabaseUtils mdbUtils;

	@Override
	public void onReceive(Context context, Intent intent) {
		// TODO Auto-generated method stub

		

		mdbUtils = new DatabaseUtils(context);

		Bundle extras = intent.getExtras();

		if(extras!=null){

			note = extras.getString("note");
			mRemId = extras.getInt("reminderId");
		}
		
		try {
			
			mdbUtils.open();
			mdbUtils.setAlarm(mRemId,"1");
			mdbUtils.close();
			
		} catch (Exception e) {
			// TODO: handle exception
			
			e.printStackTrace();
		}


		//Toast.makeText(context, "Alarm Triggered", Toast.LENGTH_LONG).show();

		Intent intent1 		= new Intent(context, AlarmDialog.class);
		intent1.putExtra("message", note);
		intent1.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		context.startActivity(intent1);
		
		
		
	}

}
