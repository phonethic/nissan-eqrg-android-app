package com.phonethics.nissanqrg;

import java.util.ArrayList;

import com.squareup.picasso.Picasso;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class StoreDetailView extends ActionBarActivity {

	ActionBar mActionBar;
	ImageView mStoreLogo;
	TextView mStoreName;
	TextView mStoreAddress;
	TextView mContactTitle;
	ListView mContactDetailsList;
	Context mContext;

	String storeLogo;
	String storeName;
	String storeAddress;

	String storePhone1;
	String storePhone2;
	String storeMobile1;
	String storeMobile2;
	String storeFax;
	String storeEmail;


	ArrayList<String> adapterList = new ArrayList<String>();
	ContactListAdapter adapter;
	Activity mActiContext;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_store_detail_view);

		mContext = this;
		mActiContext = this;
		mActionBar = getSupportActionBar();
		mActionBar.setDisplayHomeAsUpEnabled(true);
		mActionBar.setHomeButtonEnabled(true);
		mActionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#448ac5")));

		mStoreLogo = (ImageView) findViewById(R.id.storeLogo);
		mStoreName = (TextView) findViewById(R.id.storeName);
		mStoreAddress = (TextView) findViewById(R.id.storeAddress);
		mContactTitle = (TextView) findViewById(R.id.contactTitle);
		mContactDetailsList = (ListView) findViewById(R.id.contactDetailsList);

		Bundle extras	= getIntent().getExtras();

		if(extras!=null){

			storeLogo = extras.getString("storeLogo1");
			storeName = extras.getString("storeName");
			storeAddress = extras.getString("storeAddress");

			storePhone1 = extras.getString("storePhone1");
			storePhone2 = extras.getString("storePhone2");
			storeMobile1 = extras.getString("storeMobile1");
			storeMobile2 = extras.getString("storeMobile2");
			storeFax = extras.getString("storeFax");
			storeEmail = extras.getString("storeEmail");
		}

		Typeface type = Typeface.createFromAsset(mContext.getResources().getAssets(),"Multicolore.otf"); 
		mStoreName.setTypeface(type);
		mStoreAddress.setTypeface(type);
		mContactTitle.setTypeface(type);

		// Store name & address
		mStoreName.setText(storeName);
		mStoreAddress.setText(storeAddress);


		// Store logo display
		try {

			Picasso.with(mContext)
			.load(storeLogo)
			.placeholder(R.drawable.ic_launcher)
			.error(R.drawable.ic_launcher);

		} catch (Exception e) {
			// TODO: handle exception

			e.printStackTrace();
		}


		// Store contact list

		if(storePhone1.length()!=0){

			adapterList.add(storePhone1);
		}
		if(storePhone2.length()!=0){

			adapterList.add(storePhone2);
		}
		if(storeMobile1.length()!=0){

			adapterList.add(storeMobile1);
		}
		if(storeMobile2.length()!=0){

			adapterList.add(storeMobile2);
		}
		if(storeFax.length()!=0){

			adapterList.add(storeFax);
		}
		if(storeEmail.length()!=0){

			adapterList.add(storeEmail);
		}

		adapter = new ContactListAdapter(mActiContext,R.drawable.ic_launcher , adapterList);
		mContactDetailsList.setAdapter(adapter);

		mContactDetailsList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long arg3) {
				// TODO Auto-generated method stub

				if(adapterList.get(position).startsWith("+91")){

					Toast.makeText(mContext, "Call it", 0).show();
					Intent call = new Intent(android.content.Intent.ACTION_DIAL);
					call.setData(Uri.parse("tel:" + adapterList.get(position)));
					startActivity(call);
				}
				else if(adapterList.get(position).contains("@")){

					Toast.makeText(mContext, "mail it", 0).show();
					Intent email=new Intent(Intent.ACTION_SEND);
					email.putExtra("android.intent.extra.EMAIL",new String[]{adapterList.get(position)});
					email.putExtra("android.intent.extra.SUBJECT", "");
					email.setType("message/rfc822");
					startActivity(Intent.createChooser(email, "Send mail..."));
				}
				else{

					Toast.makeText(mContext, "fax it", 0).show();

				}
			}
		});
	}

	class ContactListAdapter extends ArrayAdapter<String>{

		ArrayList<String> contactList = new ArrayList<String>();
		LayoutInflater layoutInflater;
		Context context;

		public ContactListAdapter(Activity context, int resource, ArrayList<String> contactList) {
			super(context, resource);
			// TODO Auto-generated constructor stub

			this.contactList = contactList;
			layoutInflater = context.getLayoutInflater();
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return contactList.size();
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub

			if(convertView == null){


				convertView = layoutInflater.inflate(R.layout.tipslistviewlayout, null);

				ViewHolder holder = new ViewHolder();

				holder.contactText = (TextView) convertView.findViewById(R.id.tvTipsTypes);
				holder.contactType = (ImageView) convertView.findViewById(R.id.ivTipsIcons);

				convertView.setTag(holder);
			}

			ViewHolder hold = (ViewHolder) convertView.getTag();

			Typeface type = Typeface.createFromAsset(mContext.getResources().getAssets(),"Multicolore.otf"); 
			hold.contactText.setTypeface(type);

			hold.contactText.setText(contactList.get(position));
			if(adapterList.get(position).startsWith("+91")){

				hold.contactType.setImageResource(R.drawable.callicon);
			}
			else if(adapterList.get(position).contains("@")){

				hold.contactType.setImageResource(R.drawable.emailicon);
			}
			else{

				hold.contactType.setImageResource(R.drawable.faxicon);
			}


			return convertView;
		}


	}

	class ViewHolder{

		public TextView contactText;
		public ImageView contactType;

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.store_detail_view, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle presses on the action bar items

		finish();
		return true;
	}
}
