package com.phonethics.nissanqrg;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.adapters.DrawerListAdapter;
import com.listeners.StartActivity;
import com.utils.Utils;

@SuppressLint("NewApi")
public class MyFragmentActivity extends ActionBarActivity {

	private ActionBar mActionBar;
	private ListView mLvDrawer;
	private Utils mUtils;
	private DrawerLayout mDrawer;
	private int mScreenNumber;
	ActionBarDrawerToggle mActionBarToggle;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.maindrawerlayout);

		initViews();

		mActionBar.setDisplayHomeAsUpEnabled(true);
		mActionBar.setHomeButtonEnabled(false);
		mActionBar.setIcon(R.drawable.ic_drawer_icon);


		mActionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#448ac5")));

		mScreenNumber = getIntent().getExtras().getInt("screenNumber");

		StartActivity.startCustomFragment(MyFragmentActivity.this, getSupportFragmentManager().beginTransaction(), mScreenNumber);

		float[] dimen = mUtils.calculateScreenDimen();

		mLvDrawer.getLayoutParams().width = (int) (dimen[1] - 120);

		mLvDrawer.setAdapter(new DrawerListAdapter(MyFragmentActivity.this));

		/* Click listener */
		mLvDrawer.setOnItemClickListener(drawerClick);
		mDrawer.setDrawerListener(mActionBarToggle);




	}

	private void initViews(){
		mActionBar = getSupportActionBar();

		mLvDrawer = (ListView) findViewById(R.id.lvDrawer);
		mDrawer = (DrawerLayout) findViewById(R.id.drawer);

		mUtils = new Utils(MyFragmentActivity.this);
	}


	OnItemClickListener drawerClick = new OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> adapterView, View view, int position,
				long arg3) {

			if(mScreenNumber != position){
				StartActivity.startCustomFragment(MyFragmentActivity.this, 
						getSupportFragmentManager().beginTransaction(), 
						position);
				mScreenNumber = position;
			}
			else{

				if(mScreenNumber == 1){

				}
				else{

					StartActivity.startCustomFragment(MyFragmentActivity.this, 
							getSupportFragmentManager().beginTransaction(), 
							position);
					mScreenNumber = position;
				}

			}

			mDrawer.closeDrawers();
		}
	};




	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle presses on the action bar items

		mDrawer.openDrawer(Gravity.LEFT);
		if(mDrawer.isDrawerOpen(Gravity.LEFT)){
			mDrawer.closeDrawers();
		}
		return true;
	}
}
