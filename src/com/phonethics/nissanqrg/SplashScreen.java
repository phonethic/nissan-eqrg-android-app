package com.phonethics.nissanqrg;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;


import com.log.NissanLog;

public class SplashScreen extends Activity {

	private TextView mTvNissan;
	private Animation mProgressAnim;
	private ImageView mIvProgress;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.splashscreen);

		initViews();

		Typeface type = Typeface.createFromAsset(getAssets(),"BorisBlackBloxxDirty.ttf"); 
		mTvNissan.setTypeface(type);

		new Thread(new Runnable() {

			@Override
			public void run() {
				try{
					Thread.sleep(2000);

					Intent intent = new Intent(SplashScreen.this, LandingScreen.class);
					finish();
					startActivity(intent);
				}
				catch(Exception ex){
					NissanLog.nissanLogE("SplashScrren.java error", "Error in thread");
					ex.printStackTrace();
				}
			}
		}).start();

		mIvProgress.setAnimation(mProgressAnim);
	}

	private void initViews(){
		mTvNissan = (TextView) findViewById(R.id.tvNissan);
		mProgressAnim = AnimationUtils.loadAnimation(SplashScreen.this, R.anim.progressbarrotate);
		mIvProgress = (ImageView) findViewById(R.id.ivProgress);
	}

}
