package com.phonethics.nissanqrg;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.TextView;

import com.adapters.GridViewAdapter;
import com.adapters.ViewPagerAdapter;

import com.listeners.StartActivity;

public class LandingScreen extends ActionBarActivity {

	private GridView mGridView;
	private ActionBar mActionBar;
	private ViewPager mLandingScreenPager;

	private TextView mtvBullet1;
	private TextView mtvBullet2;
	private TextView mtvBullet3;
	private TextView mtvBullet4;
	private TextView mtvBullet5;
	private TextView mtvBullet6;
	private TextView mtvCurrent;
	private TextView mtvOld;

	private int mPageSelected, mPageNotSelected;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.landingscreen);

		initViews();

		changePagerIndicator(0);

		mActionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#448ac5")));

		mGridView.setAdapter(new GridViewAdapter(LandingScreen.this, 0));

		mLandingScreenPager.setAdapter(new ViewPagerAdapter(LandingScreen.this, 0));

		mLandingScreenPager.setOnPageChangeListener(new OnPageChangeListener() {

			@Override
			public void onPageSelected(int position) {
				changePagerIndicator(position);
			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {

			}

			@Override
			public void onPageScrollStateChanged(int arg0) {

			}
		});


		mGridView.setOnItemClickListener(gridClick);

	}


	/**
	 * Initialize views in LandingScreen class
	 * @author Anirudh
	 */
	private void initViews(){
		mGridView = (GridView) findViewById(R.id.landingScreenGridView);
		mActionBar = getSupportActionBar();
		mLandingScreenPager = (ViewPager) findViewById(R.id.landingScreenPager);

		mtvBullet1 = (TextView) findViewById(R.id.bullet1);
		mtvBullet2 = (TextView) findViewById(R.id.bullet2);
		mtvBullet3 = (TextView) findViewById(R.id.bullet3);
		mtvBullet4 = (TextView) findViewById(R.id.bullet4);
		mtvBullet5 = (TextView) findViewById(R.id.bullet5);
		mtvBullet6 = (TextView) findViewById(R.id.bullet6);

		mPageSelected = Color.parseColor("#FFFFFF");
		mPageNotSelected = Color.parseColor("#6f777f");
	}


	/**
	 * Changes the circle colour for view pager in LandingScreen class
	 * @param position - View pager selected page position
	 * @author Anirudh
	 */
	private void changePagerIndicator(int position){

		if(position == 0)
			mtvCurrent = mtvBullet1;
		else if(position == 1)
			mtvCurrent = mtvBullet2;
		else if(position == 2)
			mtvCurrent = mtvBullet3;
		else if(position == 3)
			mtvCurrent = mtvBullet4;
		else if(position == 4)
			mtvCurrent = mtvBullet5;
		else if(position == 5)
			mtvCurrent = mtvBullet6;

		mtvCurrent.setTextColor(mPageSelected);
		if(mtvOld != null)
			mtvOld.setTextColor(mPageNotSelected);
		mtvOld = mtvCurrent;
	}


	/*
	 * Grid click listener 
	 */
	OnItemClickListener gridClick = new OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> adapterView, View view, int position,
				long arg3) {

			StartActivity.startCustomActivity(LandingScreen.this, position);

		}
	}; 

}
