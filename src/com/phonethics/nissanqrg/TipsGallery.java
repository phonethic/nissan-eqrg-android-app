package com.phonethics.nissanqrg;

import java.util.ArrayList;

import com.fragments.TipsPagerFragment;

import android.R.string;
import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

public class TipsGallery extends ActionBarActivity {

	ViewPager mPager;
	Context mContext;
	ActionBar mActionBar;

	ArrayList<String> imgLinks;
	ArrayList<String> videoLinks;

	ProgressBar mProgressBar;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_tips_gallery);

		mContext = this;

		mActionBar = getSupportActionBar();
		mActionBar.setDisplayHomeAsUpEnabled(true);
		mActionBar.setHomeButtonEnabled(true);
		mActionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#448ac5")));


		mPager = (ViewPager) findViewById(R.id.viewPagerTips);
		mProgressBar = (ProgressBar) findViewById(R.id.galleryProgressBar);


		imgLinks = getIntent().getStringArrayListExtra("imagesLink");
		videoLinks= getIntent().getStringArrayListExtra("videoLinks");
		mPager.setAdapter(new TipsPagerAdapter(getSupportFragmentManager(), mContext, imgLinks,videoLinks));
		mPager.setOffscreenPageLimit(4);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.tips_gallery, menu);
		return true;
	}

	class TipsPagerAdapter extends FragmentPagerAdapter
	{

		Context context;
		ArrayList<String> imgLinks=null;
		ArrayList<String> videoLinks=null;

		public TipsPagerAdapter(FragmentManager fm,Context context,ArrayList<String> url,ArrayList<String> vlink) {
			super(fm);
			// TODO Auto-generated constructor stub

			this.context = context;
			this.imgLinks = url;
			this.videoLinks = vlink;


		}

		@Override
		public Fragment getItem(int arg0) {
			// TODO Auto-generated method stub
			return   TipsPagerFragment.newInstance(imgLinks.get(arg0), videoLinks.get(arg0));
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return imgLinks.size();
		}


	}

	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle presses on the action bar items

		finish();
		return true;
	}
}
