package com.phonethics.nissanqrg;

import java.util.ArrayList;

import android.R.string;

public class DealersListModel {

	private String storeId;
	private String name;
	private String address;
	private String phone1;
	private String phone2;
	private String mobile1;
	private String mobile2;
	private String fax;
	private String email;
	private String website;
	private String photo1;
	private String photo2;
	private String description;
	private String latitude;
	private String longitude;

	ArrayList<String> storeNames = new ArrayList<String>();
	ArrayList<String> storeIds = new ArrayList<String>();
	ArrayList<String> storeAddress = new ArrayList<String>();
	ArrayList<String> storePhone1 = new ArrayList<String>();
	ArrayList<String> storePhone2 = new ArrayList<String>();
	ArrayList<String> storeMobile1 = new ArrayList<String>();
	ArrayList<String> storeMobile2 = new ArrayList<String>();
	ArrayList<String> storeFax = new ArrayList<String>();
	ArrayList<String> storeEmail = new ArrayList<String>();
	ArrayList<String> storeWebsite = new ArrayList<String>();
	ArrayList<String> storePhoto1 = new ArrayList<String>();
	ArrayList<String> storePhoto2 = new ArrayList<String>();
	ArrayList<String> storeDescription = new ArrayList<String>();
	ArrayList<String> storeLatitude = new ArrayList<String>();
	ArrayList<String> storeLongitude = new ArrayList<String>();

	// store name
	public ArrayList<String> getStoreNames() {
		return storeNames;
	}

	public void setStoreNames(String names) {
		storeNames.add(names);
	}

	// store ids
	public ArrayList<String> getStoreIds() {
		return storeIds;
	}
	public void setStoreIds(String storeId) {
		storeIds.add(storeId);
	}


	//store address
	public ArrayList<String> getStoreAddress() {
		return storeAddress;
	}
	public void setStoreAddress(String storeAddress) {
		this.storeAddress.add(storeAddress);
	}

	//store phone1
	public ArrayList<String> getStorePhone1() {
		return storePhone1;
	}
	public void setStorePhone1(String storePhone1) {
		this.storePhone1.add(storePhone1);
	}


	//store phone2
	public ArrayList<String> getStorePhone2() {
		return storePhone2;
	}

	public void setStorePhone2(String storePhone2) {
		this.storePhone2.add(storePhone2);
	}

	
	//store Description
	public ArrayList<String> getStoreDescription() {
		return storeDescription;
	}
	public void setStoreDescription(String storeDescription) {
		this.storeDescription.add(storeDescription);
	}

	
	//store latitiude
	public ArrayList<String> getStoreLatitude() {
		return storeLatitude;
	}
	public void setStoreLatitude(String storeLatitude) {
		this.storeLatitude.add(storeLatitude);
	}

	
	//store longitude
	public ArrayList<String> getStoreLongitude() {
		return storeLongitude;
	}
	public void setStoreLongitude(String storeLongitude) {
		this.storeLongitude.add(storeLongitude);
	}

	
	//store photo1
	public ArrayList<String> getStorePhoto1() {
		return storePhoto1;
	}
	public void setStorePhoto1(String storePhoto1) {
		this.storePhoto1.add(storePhoto1);
	}

	
	//store photo2
	public ArrayList<String> getStorePhoto2() {
		return storePhoto2;
	}
	public void setStorePhoto2(String storePhoto2) {
		this.storePhoto2.add(storePhoto2);
	}

		
	//store website
	public ArrayList<String> getStoreWebsite() {
		return storeWebsite;
	}
	public void setStoreWebsite(String storeWebsite) {
		this.storeWebsite.add(storeWebsite);
	}

	
	//store email
	public ArrayList<String> getStoreEmail() {
		return storeEmail;
	}
	public void setStoreEmail(String storeEmail) {
		this.storeEmail.add(storeEmail);
	}
	
	
	//store mobile1
	public ArrayList<String> getStoreMobile1() {
		return storeMobile1;
	}
	public void setStoreMobile1(String storeMobile1) {
		this.storeMobile1.add(storeMobile1);
	}
	
	
	//store mobile2
	public ArrayList<String> getStoreMobile2() {
		return storeMobile2;
	}
	public void setStoreMobile2(String storeMobile2) {
		this.storeMobile2.add(storeMobile2);
	}
	
	
	//store fax
	public ArrayList<String> getStoreFax() {
		return storeFax;
	}
	public void setStoreFax(String storeFax) {
		this.storeFax.add(storeFax);
	}
	
}
