package com.phonethics.nissanqrg;

import uk.co.senab.photoview.HackyViewPager;

import com.adapters.ViewPagerAdapter;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.widget.TextView;

public class BigImage extends Activity {
	
	private HackyViewPager mPager;
	private TextView mTvPagerTitle;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.bigimages);
		
		initViews();
		
		String adapterType = getIntent().getStringExtra("adapterType");
		
		/*if(adapterType.equalsIgnoreCase("Tips_Maintainance")){
			
		}*/
		
		mPager.setAdapter(new ViewPagerAdapter(BigImage.this, 1));
	}
	
	
	private void initViews(){
		mPager = (HackyViewPager) findViewById(R.id.bigImagePager);
		mTvPagerTitle = (TextView) findViewById(R.id.tvPagerTitle);
	}
	
	

}
