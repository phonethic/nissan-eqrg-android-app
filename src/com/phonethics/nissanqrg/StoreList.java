package com.phonethics.nissanqrg;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.SocketException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.http.ConnectionClosedException;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.xml.sax.InputSource;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.adapters.DealerListAdapter;
import com.log.NissanLog;
import com.phonethics.network.NetworkCheck;

public class StoreList extends ActionBarActivity{

	private ActionBar mActionBar;
	ExpandableListView mDealerList;
	final String mUrl = "http://stage.phonethics.in/proj/neon/neon_stores.php";
	final String mFilePath = "/sdcard/NissanQRG/dealersdata.txt";
	DefaultHttpClient 			httpClient;
	HttpPost 					httpPost;
	HttpResponse				httpRes;
	HttpEntity					httpEnt;
	NetworkCheck mNetAvailable; 
	ProgressBar mProgressBar;
	Context context;
	String mXML;
	DealersListModel mDealerData = null;
	ArrayList<DealersListModel> cityList = new ArrayList<DealersListModel>();
	ArrayList<String> cityNames;
	ArrayList<DealersListModel> citWiseStores = new ArrayList<DealersListModel>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_store_list);

		context = this;
		mNetAvailable = new NetworkCheck(context);

		mActionBar = getSupportActionBar();
		mActionBar.setDisplayHomeAsUpEnabled(true);
		mActionBar.setHomeButtonEnabled(true);
		mActionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#448ac5")));

		cityNames = new ArrayList<String>();
		mDealerList = (ExpandableListView) findViewById(R.id.dealersList);
		mProgressBar = (ProgressBar) findViewById(R.id.dealorLocatorProgressBar);

		File file = new File(mFilePath);
		if(file.exists()){

			try {

				mProgressBar.setVisibility(View.VISIBLE);
				parseXML();

			} catch (XmlPullParserException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		else{


			if(mNetAvailable.isNetworkAvailable()){

				DealerListAsyncTask download = new DealerListAsyncTask();
				download.execute(mUrl);

			}
			else{

				Toast.makeText(context, "No internet connection", Toast.LENGTH_SHORT).show();
			}
		}

		mDealerList.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
			@Override
			public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
				// Doing nothing
				return true;
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.store_list, menu);
		return true;
	}

	private class DealerListAsyncTask extends AsyncTask<String, String, String>{

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub


			// TODO Auto-generated method stub

			int count;
			mProgressBar.setVisibility(View.VISIBLE);

			try {
				httpClient 	=	new DefaultHttpClient();
				httpPost 	= 	new HttpPost(mUrl);

				httpRes		=	httpClient.execute(httpPost);

				httpEnt		=	httpRes.getEntity();
				mXML 		=	EntityUtils.toString(httpEnt);

				DocumentBuilderFactory 	dbf	=	DocumentBuilderFactory.newInstance();
				dbf.newDocumentBuilder();
				InputSource		is	= new InputSource();
				is.setCharacterStream(new StringReader(mXML));



				URL url = new URL(params[0]);
				URLConnection connection = url.openConnection();
				connection.connect();

				int lenghtOfFile = connection.getContentLength();

				// input stream to read file - with 8k buffer
				InputStream input = new BufferedInputStream(url.openStream(), 8192);

				// Output stream to write file
				try
				{
					/*
					 * creating directory to store xml 
					 */
					File myDirectory = new File(Environment.getExternalStorageDirectory(), "NissanQRG");

					if(!myDirectory.exists()) {                                 
						myDirectory.mkdirs();
					}
				}catch(Exception ex)
				{
					ex.printStackTrace();
				}
				FileOutputStream output = new FileOutputStream(mFilePath);


				byte data[] = new byte[1024];
				long total = 0;

				while (   ((count = input.read(data)) != -1)) {


					total += count;

					output.write(data, 0, count);
				}

				output.flush();
				// closing streams
				output.close();
				input.close();


			}catch(SocketException socketException){


			}catch(ConnectionClosedException con){

			}catch(ConnectTimeoutException timeOut){


			}catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			catch (Exception e){

			}


			return null;

		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);


			try {

				parseXML();

			} catch (XmlPullParserException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException ex) {
				// TODO Auto-generated catch block
				ex.printStackTrace();
			}


		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();

		}
	}

	private void parseXML() throws XmlPullParserException,IOException{

		XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
		factory.setNamespaceAware(true);
		XmlPullParser xpp = factory.newPullParser();

		try {

			File file = new File(mFilePath);
			FileInputStream fis = new FileInputStream(file);
			xpp.setInput(new InputStreamReader(fis));

			int eventType = xpp.getEventType();

			while(eventType!=XmlPullParser.END_DOCUMENT){

				String tagName = xpp.getName();

				switch (eventType) {

				case XmlPullParser.START_DOCUMENT:

					break;

				case XmlPullParser.START_TAG:



					if(tagName.equalsIgnoreCase("city")){

						NissanLog.nissanLogV("CI NA","CI NA" +  xpp.getAttributeValue(null, "name"));
						String cityName = xpp.getAttributeValue(null, "name");
						cityNames.add(cityName);

					}

					if("store".equalsIgnoreCase(xpp.getName())) {

						NissanLog.nissanLogV("CI NA","CI NA" +  xpp.getAttributeValue(null, "name"));

						mDealerData.setStoreIds(xpp.getAttributeValue(null, "id"));
						mDealerData.setStoreNames(xpp.getAttributeValue(null, "name"));
						mDealerData.setStoreAddress(xpp.getAttributeValue(null, "address"));
						mDealerData.setStorePhone1(xpp.getAttributeValue(null, "phone1"));
						mDealerData.setStorePhone2(xpp.getAttributeValue(null, "phone2"));
						mDealerData.setStoreMobile1(xpp.getAttributeValue(null, "mobile1"));
						mDealerData.setStoreMobile2(xpp.getAttributeValue(null, "mobile2"));
						mDealerData.setStoreFax(xpp.getAttributeValue(null, "fax"));
						mDealerData.setStoreEmail(xpp.getAttributeValue(null, "email"));
						mDealerData.setStoreWebsite(xpp.getAttributeValue(null, "website"));
						mDealerData.setStorePhoto1(xpp.getAttributeValue(null, "photo1"));
						mDealerData.setStorePhoto2(xpp.getAttributeValue(null, "photo2"));
						mDealerData.setStoreDescription(xpp.getAttributeValue(null, "description"));
						mDealerData.setStoreLatitude(xpp.getAttributeValue(null, "latitude"));
						mDealerData.setStoreLongitude(xpp.getAttributeValue(null, "longitude"));

						//	citWiseStores.add(mDealerData);
					}
					else if("city".equals(xpp.getName())) {

						mDealerData = new DealersListModel();
					}

					break;


				case XmlPullParser.END_TAG:

					Log.d("ENDTAGS","ENDTAGS" + tagName);

					if("city".equals(xpp.getName())) {

						cityList.add(mDealerData);

					}

					break;

				default:
					break;
				}

				eventType = xpp.next();
			}

			mProgressBar.setVisibility(View.GONE);

			DealerListAdapter adapter = new DealerListAdapter(context, cityNames, cityList);
			mDealerList.setAdapter(adapter);
			mDealerList.setGroupIndicator(null);

			for(int i=0;i<adapter.getGroupCount();i++){
				mDealerList.expandGroup(i);
			}

			for(int m=0; m< cityList.size();m++){

				NissanLog.nissanLogV("STORES ","STORES " + cityList.get(m).getStoreNames());
			}


		} catch (IOException e) {
			// TODO: handle exception

			e.printStackTrace();

		} catch (XmlPullParserException ex) {
			// TODO: handle exception

			ex.printStackTrace();
		}

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle presses on the action bar items

		finish();
		return true;
	}
}
