package com.listeners;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.widget.Toast;



import com.fragments.ContactUs;
import com.fragments.FindMyCar;
import com.fragments.MyCar;
import com.fragments.NissaneQRG;
import com.fragments.Reminder;
import com.fragments.Tips;
import com.fragments.TrafficRules;
import com.log.NissanLog;
import com.phonethics.nissanqrg.MyFragmentActivity;
import com.phonethics.nissanqrg.R;

public class StartActivity {


	/**
	 * Custom activity caller
	 * @param context - Activity context
	 * @param position - Activity Number
	 * @author Anirudh
	 */

	private static Fragment mOldFragment; 
	public static void startCustomActivity(Context context, int position){
		Intent intent = new Intent(context, MyFragmentActivity.class);
		intent.putExtra("screenNumber", position);
		context.startActivity(intent);
	}

	public static void destroyFragment (FragmentTransaction ft, Fragment frObject){
		try{
			ft.detach(frObject);
			ft.commit();
		}
		catch (Exception e) {
			NissanLog.nissanLogE("StartActivity", "Destroy fragment error");
			e.printStackTrace();
		}
	}

	public static void startCustomFragment(Context context, FragmentTransaction ft, int position){
		mOldFragment = null;
		switch (position) {  
		case 0:{ //Nissan eQRG
			mOldFragment = new NissaneQRG();
			ft.replace(R.id.fragment, new NissaneQRG());
			ft.commit();

			break;
		}
		case 1:{  //Find My Car

			mOldFragment = new FindMyCar();
			ft.replace(R.id.fragment, mOldFragment);
			ft.commit();

			break;
		}

		case 2: {  //Reminder
			mOldFragment = new Reminder();
			ft.replace(R.id.fragment,mOldFragment);
			ft.commit();
			mOldFragment = new Reminder();
			break;
		}

		case 3: {  //Traffic Rules
			mOldFragment = new TrafficRules();
			ft.replace(R.id.fragment, mOldFragment);
			ft.commit();
			mOldFragment = new TrafficRules();
			break;
		}

		case 4:{  //Tips
			mOldFragment = new Tips();
			ft.replace(R.id.fragment, mOldFragment);
			ft.commit();
			mOldFragment = new Tips();
			break;
		}

		case 5:{  //My Car
			mOldFragment = new MyCar();
			ft.replace(R.id.fragment, mOldFragment);
			ft.commit();
			mOldFragment = new MyCar();
			break;
		}

		case 6:{  //Contact Us
			mOldFragment = new ContactUs();
			ft.replace(R.id.fragment, mOldFragment);
			ft.commit();
			mOldFragment = new ContactUs();
			break;
		}

		default:
			Toast.makeText(context, "Coming soon", Toast.LENGTH_LONG).show();
			break;
		}
	}


}

class NoSuchFragmentException extends Exception{

}
