package com.utils;

import java.util.ArrayList;

import com.log.NissanLog;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.widget.Toast;

public class DBUtils {

	private SQLiteDatabase msqlDatabase;

	public DBUtils(Context mContext){
		msqlDatabase = mContext.openOrCreateDatabase("Nissan.db", 1, null);
	}


	/**
	 * My Car frgament = table name is MyCar
	 **/

	public void createTable(String tableName, String[] tableColumn){
		
		String CREATE_TABLE = "CREATE TABLE IF NOT EXISTS "+tableName+ "(";
		for(int iVal=0; iVal<tableColumn.length; iVal++){
				CREATE_TABLE = CREATE_TABLE + ""+tableColumn[iVal] + " VARCHAR";
				if(iVal < tableColumn.length-1){
					CREATE_TABLE = CREATE_TABLE + ", ";
				} else {
					CREATE_TABLE = CREATE_TABLE + ");";
				}
		}

		msqlDatabase.execSQL(CREATE_TABLE);
	}


	public void insertData(ArrayList<String> carDetails, String[] tableColumn){

		String SELECT_FROM_TABLE = "SELECT * FROM MyCar";


		Cursor cursor = msqlDatabase.rawQuery(SELECT_FROM_TABLE, null);

		if(cursor.getCount() > 1){
			ContentValues values = new ContentValues();
			for(int iVal=0; iVal<tableColumn.length; iVal++){
				values.put(tableColumn[iVal], carDetails.get(iVal));
			}
			msqlDatabase.update("MyCar", values, "id=1", null);
		} else{
			ContentValues values = new ContentValues();
			for(int iVal=0; iVal<tableColumn.length; iVal++){
				values.put(tableColumn[iVal], carDetails.get(iVal));	
			}

			msqlDatabase.insert("MyCar", null, values);
		}

	}

	public ArrayList<String> getData(String tableName, String[] tableColumn){
		ArrayList<String> data = new ArrayList<String>();
		String GET_DATA = "SELECT * FROM "+tableName;

		Cursor mCursor = msqlDatabase.rawQuery(GET_DATA, null);

		if(mCursor.getCount() > 0){
			mCursor.moveToFirst();
			while(true){
				if(mCursor.isAfterLast()) break;
				
				for(int iVal=0; iVal<tableColumn.length; iVal++){
					NissanLog.nissanLogI("DATABASE VALUE", "TABLE VALUE: "+mCursor.getString(iVal));
					data.add(""+mCursor.getString(iVal));
				}
				mCursor.moveToNext();
			}
		}
		
		return data;
	}
}
