package com.utils;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore.Images;
import android.util.Log;

public class CameraImageSave {

	private String fileName = "myCar.jpg";


	public void saveBitmapToFile(Bitmap bitmap){
		ByteArrayOutputStream bytes = new ByteArrayOutputStream();
		bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);

		//String filePath = "/sdcard/temp_photo.jpg";
		File f = new File(Environment.getExternalStorageDirectory()
				+ File.separator + fileName);

		try{
			f.createNewFile();
			FileOutputStream fo = new FileOutputStream(f);
			fo.write(bytes.toByteArray());
			fo.close();
		}
		catch(Exception e){ Log.e("Bitmap error","File not found to save image"); }
	}


	public void deleteFromFile(){

		File f = new File(Environment.getExternalStorageDirectory()
				+ File.separator + fileName);

		f.delete();

	}

	public Bitmap getBitmapFromFile(){
		Bitmap bitmap = null;
		String filePath = Environment.getExternalStorageDirectory()
				+ File.separator + fileName;

		try{
			FileInputStream in = new FileInputStream(filePath);
			BufferedInputStream buf = new BufferedInputStream(in);
			bitmap = BitmapFactory.decodeStream(buf);
		}
		catch(Exception e){
			Log.e("Exception","getBitmapFromFile() -> CameraSaveImage.java");
			e.printStackTrace();
		}

		return bitmap;
	}

	public String getImagePath(){
		return Environment.getExternalStorageDirectory()
				+ File.separator + fileName;
	}

	public Uri getImageUri(Context inContext, Bitmap inImage) {
		ByteArrayOutputStream bytes = new ByteArrayOutputStream();
		inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
		String path = Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
		return Uri.parse(path);
	} 

}
