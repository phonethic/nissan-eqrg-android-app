package com.adapters;

import java.util.ArrayList;

import com.phonethics.nissanqrg.R;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;



public class NissaneqrgFeatureListAdapter extends NissanAdapter {

	private Context mContext;
	private ArrayList<String> mItems;

	public NissaneqrgFeatureListAdapter(Context context, ArrayList<String> items){
		mContext = context;
		mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		//miAdapterCount = mAdapterData.eQRGFeatureList.length;
		miAdapterCount = items.size();
		mItems = items;
	}


	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		Holder holder;
		if(convertView == null){

			holder = new Holder();

			convertView = mInflater.inflate(R.layout.nissaneqrgfeaturelistlayout, null);

			holder.tv = (TextView) convertView.findViewById(R.id.tvNissanFeatureNumber);
			holder.tvContactUsTitle = (TextView) convertView.findViewById(R.id.tvNissanFeatureName);

			Typeface type = Typeface.createFromAsset(mContext.getResources().getAssets(),"Multicolore.otf"); 
			holder.tvContactUsTitle.setTypeface(type);

			convertView.setTag(holder);
		}

		holder = (Holder) convertView.getTag();

		//		holder.tvContactUsTitle.setText(mAdapterData.eQRGFeatureList[position]);

		holder.tvContactUsTitle.setText(mItems.get(position));
		holder.tv.setText("" + (position+1));

		return convertView;
	}


	class Holder{
		TextView tv;
		TextView tvContactUsTitle;
	}
}
