package com.adapters;

import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.log.NissanLog;
import com.phonethics.nissanqrg.DealersListModel;
import com.phonethics.nissanqrg.R;
import com.phonethics.nissanqrg.StoreDetailView;

public class DealerListAdapter extends BaseExpandableListAdapter {

	ArrayList<String> cityNames;
	Context context;
	ArrayList<DealersListModel> cityList;
	LayoutInflater inflater;

	public DealerListAdapter(Context context, ArrayList<String> cityNames,
			ArrayList<DealersListModel> cityList) {
		// TODO Auto-generated constructor stub

		this.context = context;
		this.cityNames = cityNames;
		this.cityList = cityList;
		inflater = LayoutInflater.from(context);
	}

	@Override
	public Object getChild(int groupPosition, int childPosition) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getChildView(final int groupPosition, final int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub

		if (convertView == null) {
			convertView = inflater.inflate(R.layout.dealerlistchildlayout, parent,false);
		}
		TextView text_name = (TextView) convertView.findViewById(R.id.dealerName);
		TextView text_add = (TextView) convertView.findViewById(R.id.dealerAddress);

		Typeface type = Typeface.createFromAsset(context.getResources().getAssets(),"Multicolore.otf"); 
		text_name.setTypeface(type);
		text_add.setTypeface(type);

		text_name.setText(cityList.get(groupPosition).getStoreNames().get(childPosition));
		text_add.setText(cityList.get(groupPosition).getStoreAddress().get(childPosition));

		convertView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				String storeName = cityList.get(groupPosition).getStoreNames().get(childPosition);
				NissanLog.nissanLogV("StoreSize ","StoreSize " + cityList.get(groupPosition).getStorePhoto1().size());
				String storeAddress = cityList.get(groupPosition).getStoreAddress().get(childPosition);
				String storeLogo1 = cityList.get(groupPosition).getStorePhoto1().get(childPosition);
				String storeLogo2 = cityList.get(groupPosition).getStorePhoto2().get(childPosition);
				String storePhone1 = cityList.get(groupPosition).getStorePhone1().get(childPosition);
				String storePhone2 = cityList.get(groupPosition).getStorePhone2().get(childPosition);
				String storeMobile1 = cityList.get(groupPosition).getStoreMobile1().get(childPosition);
				String storeMobile2 = cityList.get(groupPosition).getStoreMobile2().get(childPosition);
				String storeFax = cityList.get(groupPosition).getStoreFax().get(childPosition);
				String storeEmail = cityList.get(groupPosition).getStoreEmail().get(childPosition);
				String storeWebsite = cityList.get(groupPosition).getStoreWebsite().get(childPosition);
				String storeDescription = cityList.get(groupPosition).getStoreDescription().get(childPosition);
				String storeLatitude = cityList.get(groupPosition).getStoreLatitude().get(childPosition);
				String storeLongitude = cityList.get(groupPosition).getStoreLongitude().get(childPosition);

				Intent intent = new Intent(context, StoreDetailView.class);
				intent.putExtra("storeName", storeName);
				intent.putExtra("storeAddress", storeAddress);
				intent.putExtra("storeLogo1", storeLogo1);
				intent.putExtra("storeLogo2", storeLogo2);
				intent.putExtra("storePhone1", storePhone1);
				intent.putExtra("storePhone2", storePhone2);
				intent.putExtra("storeMobile1", storeMobile1);
				intent.putExtra("storeMobile2", storeMobile2);
				intent.putExtra("storeFax", storeFax);
				intent.putExtra("storeEmail", storeEmail);
				intent.putExtra("storeWebsite", storeWebsite);
				intent.putExtra("storeDescription", storeDescription);
				intent.putExtra("storeLatitude", storeLatitude);
				intent.putExtra("storeLongitude", storeLongitude);
				context.startActivity(intent);

				;}
		});

		return convertView;
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		// TODO Auto-generated method stub
		return cityList.get(groupPosition).getStoreAddress().size();
	}

	@Override
	public Object getGroup(int groupPosition) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getGroupCount() {
		// TODO Auto-generated method stub
		return cityList.size();
	}

	@Override
	public long getGroupId(int groupPosition) {
		// TODO Auto-generated method stub
		return groupPosition;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub

		if (convertView == null) {
			convertView = inflater.inflate(R.layout.dealerlistparentlayout, parent,false);
		}
		TextView textView = (TextView) convertView.findViewById(R.id.city);

		Typeface type = Typeface.createFromAsset(context.getResources().getAssets(),"Multicolore.otf");
		textView.setTypeface(type);
		textView.setText(cityNames.get(groupPosition));

		return convertView;
	}

	@Override
	public boolean hasStableIds() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		// TODO Auto-generated method stub
		return false;
	}

}
