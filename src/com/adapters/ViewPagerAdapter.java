package com.adapters;

import uk.co.senab.photoview.PhotoView;
import uk.co.senab.photoview.PhotoViewAttacher;
import uk.co.senab.photoview.PhotoViewAttacher.OnMatrixChangedListener;
import uk.co.senab.photoview.PhotoViewAttacher.OnPhotoTapListener;
import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.os.Parcelable;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.phonethics.nissanqrg.R;


public class ViewPagerAdapter extends android.support.v4.view.PagerAdapter {

	Context context;
	int screenNumber;
	LayoutInflater inflater;
	AdapterData pagerAdapterData;

	PhotoViewAttacher mPhotoAttacher;

	ImageView iv;
	TextView tv;


	public ViewPagerAdapter(Context context, int screenNumber){
		this.context = context;
		this.screenNumber = screenNumber;

		inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		pagerAdapterData = new AdapterData();

	}
	

	@Override
	public int getCount() {
		if(screenNumber == 0)
			return 6;
		else if(screenNumber == 1)
			return 2;

		return 0;
	}


	@SuppressLint("NewApi")
	@Override
	public Object instantiateItem(View container, int position) {

		View view = null;
		if(screenNumber == 0){
			view = inflater.inflate(R.layout.landingscreenviewpagerlayout, null);

			iv = (ImageView) view.findViewById(R.id.ivLandingScreenPager);
			tv = (TextView) view.findViewById(R.id.tvLandingScreenPager);


			Typeface type = Typeface.createFromAsset(context.getResources().getAssets(),"BorisBlackBloxxDirty.ttf"); 
			tv.setTypeface(type);

			if(android.os.Build.VERSION.SDK_INT >= 14)
				tv.setAlpha(0.5f);
			else
				tv.setBackgroundColor(Color.parseColor("#99FFFFFF"));

			iv.setImageResource(pagerAdapterData.landingScreenPagerImage[position]);
			tv.setText(pagerAdapterData.landingScreenPagerText[position]);
		}

		if(screenNumber == 1){
			view = inflater.inflate(R.layout.bigimagespagerlayout, null);

			iv = (ImageView) view.findViewById(R.id.ivBigImagePager);

			iv.setImageResource(R.drawable.hotspot);
			mPhotoAttacher = new PhotoViewAttacher(iv);
			mPhotoAttacher.setOnPhotoTapListener(new PhotoTapListener());
		}


		((ViewPager) container).addView(view, 0);
		return view;
	}


	@Override
	public void destroyItem(View arg0, int arg1, Object arg2) {
		((ViewPager) arg0).removeView((View) arg2);
		//mPhotoAttacher.cleanup();
	}

	@Override
	public boolean isViewFromObject(View arg0, Object arg1) {
		return arg0 == ((View) arg1);
	}

	@Override
	public Parcelable saveState() {
		return null;
	}


	private class PhotoTapListener implements OnPhotoTapListener {

		@Override
		public void onPhotoTap(View view, float x, float y) {
			float xPercentage = x * 100f;
			float yPercentage = y * 100f;

			/*Toast.makeText(context,
					"Clicked on 1", Toast.LENGTH_SHORT).show();*/

			if(((xPercentage>90) && (xPercentage<95)) && ((yPercentage >88) && (yPercentage < 98))){
				Toast.makeText(context,
						"Clicked on 1", Toast.LENGTH_SHORT).show();


			}

			else if(((xPercentage>75) && (xPercentage<84)) && ((yPercentage >75) && (yPercentage < 84))){
				Toast.makeText(context,
						"Clicked on 2", Toast.LENGTH_SHORT).show();

			}

			else if(((xPercentage>9) && (xPercentage<15)) && ((yPercentage >82) && (yPercentage < 90))){
				Toast.makeText(context,
						"Clicked on 3", Toast.LENGTH_SHORT).show();

				Log.d("Coordinates","X-Coord " + xPercentage);
				Log.d("Coordinates","Y-Coord " + yPercentage);
			}

			else if(((xPercentage>5) && (xPercentage<18)) && ((yPercentage >2) && (yPercentage < 13))){
				Toast.makeText(context,
						"Clicked on 4", Toast.LENGTH_SHORT).show();

				Log.d("Coordinates","X-Coord " + xPercentage);
				Log.d("Coordinates","Y-Coord " + yPercentage);
			}

			//			Log.d("Coordinates","X-Coord " + xPercentage);
			//			Log.d("Coordinates","Y-Coord " + yPercentage);
		}
	}

}
