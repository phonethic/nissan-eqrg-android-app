package com.adapters;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.SocketException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.http.ConnectionClosedException;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.xml.sax.InputSource;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.log.NissanLog;
import com.phonethics.network.NetworkCheck;
import com.phonethics.nissanqrg.R;
import com.phonethics.nissanqrg.SectionClass;
import com.squareup.picasso.Picasso;



public class TipsContactListViewAdapter extends NissanAdapter {

	private Context mContext;

	//0 = tips ; 1 = ContactUs
	private int miScreen;


	final String url = "http://stage.phonethics.in/proj/neon/neon_tips.php";
	String filePath = "/sdcard/NissanQRG/tips.txt";
	DefaultHttpClient 			httpClient;
	HttpPost 					httpPost;
	HttpResponse				httpRes;
	HttpEntity					httpEnt;
	String xml;

	public List<SectionClass> allWays = new ArrayList<SectionClass>();
	SectionClass sectionObj = null;

	NetworkCheck netAvailable; 
	ProgressBar pBar;

	private ProgressDialog mProgressDialog;


	public TipsContactListViewAdapter(Context context, int screen, ProgressBar pBar) {

		mContext = context;
		miScreen = screen;
		mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		netAvailable = new NetworkCheck(context);
		this.pBar = pBar;


		if(miScreen == 0){

		
			//to download file and parse

			if(netAvailable.isNetworkAvailable()){

				mProgressDialog=new ProgressDialog(mContext);
				mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
				mProgressDialog.setTitle("Nissan QRG");
				mProgressDialog.setMessage("Loading please wait");
				mProgressDialog.show();

				MyAsyncTask download = new MyAsyncTask();
				download.execute(url);

			}
			else{

				File file = new File(filePath);

				if(file.exists()){


					//Toast.makeText(context, "No internet connection but file is there", 0).show();
					try {
						parseXML();
					} catch (XmlPullParserException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				else{

					Toast.makeText(context, "No internet connection", 0).show();
				}
			}


		}

		else if(miScreen == 1)
			miAdapterCount = mAdapterData.contactUsText.length;
	}


	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		Holder holder = null;
		if(convertView == null){

			if(miScreen == 0) { //Tips
				holder = new Holder();
				convertView = mInflater.inflate(R.layout.tipslistviewlayout, null);

				holder.tv = (TextView) convertView.findViewById(R.id.tvTipsTypes);
				holder.iv = (ImageView) convertView.findViewById(R.id.ivTipsIcons);

				Typeface type = Typeface.createFromAsset(mContext.getResources().getAssets(),"Multicolore.otf"); 
				holder.tv.setTypeface(type);
			}

			else if(miScreen == 1) { //ContactUs
				holder = new Holder();
				convertView = mInflater.inflate(R.layout.contactuslistviewlayout, null);

				holder.tv = (TextView) convertView.findViewById(R.id.tvConnectText);
				holder.iv = (ImageView) convertView.findViewById(R.id.ivConnectIcon);
				holder.tvContactUsTitle = (TextView) convertView.findViewById(R.id.tvContactTitle);

				Typeface type = Typeface.createFromAsset(mContext.getResources().getAssets(),"Multicolore.otf"); 
				holder.tv.setTypeface(type);
				holder.tvContactUsTitle.setTypeface(type);
			}

			convertView.setTag(holder);
		}

		holder = (Holder) convertView.getTag();

		if(miScreen == 0) { //Tips

			//			NissanLog.nissanLogV("NAMES","NAMES TA" + mAdapterData.sectionNameAr.get(position));
			holder.tv.setText(mAdapterData.sectionNameAr.get(position));
			//holder.iv.setImageResource(mAdapterData.tipsListIcon[position]);

			try {

				Picasso.with(mContext).load(mAdapterData.thumbLinkAr.get(position))
				.error(R.drawable.ic_launcher)
				.into(holder.iv);

			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}

		}
		else if(miScreen == 1) { //Contact us
			if(position == 0 || position == 4 || position == 7){
				holder.tvContactUsTitle.setVisibility(View.VISIBLE);
				holder.tv.setVisibility(View.GONE);
				holder.iv.setVisibility(View.GONE);
				holder.tvContactUsTitle.setText(mAdapterData.contactUsText[position]);
			}
			else{
				holder.tvContactUsTitle.setVisibility(View.GONE);
				holder.tv.setVisibility(View.VISIBLE);
				holder.iv.setVisibility(View.VISIBLE);
				holder.tv.setText(mAdapterData.contactUsText[position]);
				holder.iv.setImageResource(mAdapterData.contactUsIcon[position]);
			}
		}

		return convertView;
	}

	class Holder{
		ImageView iv;
		TextView tv;

		TextView tvContactUsTitle;
	}


	// to download tips xml

	private class MyAsyncTask extends AsyncTask<String, String, String>{
		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub

			int count;
			//pBar.setVisibility(View.VISIBLE);
			//showProgressDialog();

			try {
				httpClient 	=	new DefaultHttpClient();
				httpPost 	= 	new HttpPost(url);

				httpRes		=	httpClient.execute(httpPost);

				httpEnt		=	httpRes.getEntity();
				xml 		=	EntityUtils.toString(httpEnt);

				DocumentBuilderFactory 	dbf	=	DocumentBuilderFactory.newInstance();
				dbf.newDocumentBuilder();
				InputSource		is	= new InputSource();
				is.setCharacterStream(new StringReader(xml));



				URL url = new URL(params[0]);
				URLConnection connection = url.openConnection();
				connection.connect();

				int lenghtOfFile = connection.getContentLength();

				// input stream to read file - with 8k buffer
				InputStream input = new BufferedInputStream(url.openStream(), 8192);

				// Output stream to write file
				try
				{
					/*
					 * creating directory to store xml 
					 */
					File myDirectory = new File(Environment.getExternalStorageDirectory(), "NissanQRG");

					if(!myDirectory.exists()) {                                 
						myDirectory.mkdirs();
					}
				}catch(Exception ex)
				{
					ex.printStackTrace();
				}
				FileOutputStream output = new FileOutputStream(filePath);


				byte data[] = new byte[1024];
				long total = 0;

				while (   ((count = input.read(data)) != -1)) {


					total += count;

					output.write(data, 0, count);
				}

				output.flush();
				// closing streams
				output.close();
				input.close();


			}catch(SocketException socketException){


			}catch(ConnectionClosedException con){

			}catch(ConnectTimeoutException timeOut){


			}catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			catch (Exception e){

			}


			return null;
		}



		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);


			try {
				parseXML();

				//				miAdapterCount = mAdapterData.sectionNameAr.size();
				//				notifyDataSetChanged();

				for(int i=0;i<miAdapterCount;i++){

					NissanLog.nissanLogV("Getting","Getting " + mAdapterData.sectionNameAr.get(i));
				}
			} catch (XmlPullParserException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException ex) {
				// TODO Auto-generated catch block
				ex.printStackTrace();
			}


		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();

		}
	}

	private void parseXML() throws XmlPullParserException,IOException{

		//Toast.makeText(context, "INSIDETRY", 0).show();


		XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
		factory.setNamespaceAware(true);
		XmlPullParser xpp = factory.newPullParser();



		try {


			File file = new File(filePath);
			FileInputStream fis = new FileInputStream(file);
			xpp.setInput(new InputStreamReader(fis));

			int eventType = xpp.getEventType();

			while(eventType!=XmlPullParser.END_DOCUMENT){

				String tagName = xpp.getName();

				switch (eventType) {

				case XmlPullParser.START_DOCUMENT:

					break;

				case XmlPullParser.START_TAG:

					//String tagName = xpp.getName();

					//Log.d("TAGS","TAGS" +  tagName);

					if(tagName.equalsIgnoreCase("section")){

						String thumb = xpp.getAttributeValue(null, "ilink");

						String sectionName = xpp.getAttributeValue(null, "name");

						mAdapterData.setSectionName(sectionName);
						mAdapterData.setThumbLink(thumb);

					}

					if("photo".equals(xpp.getName())) {

						sectionObj.setIdPhoto(xpp.getAttributeValue(null, "id"));
						sectionObj.setIdilink(xpp.getAttributeValue(null, "ilink"));
						sectionObj.setIdvlink(xpp.getAttributeValue(null, "vlink"));
					}
					else if("section".equals(xpp.getName())) {

						sectionObj = new SectionClass();
					}

					break;


				case XmlPullParser.END_TAG:

					Log.d("ENDTAGS","ENDTAGS" + tagName);

					if("section".equals(xpp.getName())) {

						allWays.add(sectionObj);

					}

					break;

				default:
					break;
				}

				eventType = xpp.next();
			}

			//pBar.setVisibility(View.GONE);
			if(mProgressDialog.isShowing()){

				mProgressDialog.dismiss();
			}
			miAdapterCount = mAdapterData.sectionNameAr.size();
			notifyDataSetChanged();

		} catch (IOException e) {
			// TODO: handle exception

			e.printStackTrace();

		} catch (XmlPullParserException ex) {
			// TODO: handle exception

			ex.printStackTrace();
		}

	}

	private void showProgressDialog(){
		mProgressDialog=new ProgressDialog(mContext);
		mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		mProgressDialog.setTitle("Nissan QRG");
		mProgressDialog.setMessage("Loading please wait");
		mProgressDialog.show();
	}

}
