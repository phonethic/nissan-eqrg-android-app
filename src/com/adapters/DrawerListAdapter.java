package com.adapters;

import com.phonethics.nissanqrg.R;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;



public class DrawerListAdapter extends NissanAdapter {

	private Context mContext;


	public DrawerListAdapter(Context context){
		mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		mContext = context;

		/* Set adapter count */
		miAdapterCount = mAdapterData.landingScreenGridName.length;
	}


	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		Holder holder = null;

		if(convertView == null){

			holder = new Holder();
			convertView = mInflater.inflate(R.layout.drawerlistviewlayout, null);

			holder.iv = (ImageView) convertView.findViewById(R.id.ivDrawericon);
			holder.tv = (TextView) convertView.findViewById(R.id.tvDrawericon);

			//			Typeface type = Typeface.createFromAsset(mContext.getResources().getAssets(),"RobotoThin.ttf"); 
			Typeface type = Typeface.createFromAsset(mContext.getResources().getAssets(),"Multicolore.otf");
			holder.tv.setTypeface(type);

			convertView.setTag(holder);
		}

		holder = (Holder)convertView.getTag();

		holder.iv.setImageResource(mAdapterData.landingScreenGridIcons[position]);
		holder.tv.setText(mAdapterData.landingScreenGridName[position]);

		return convertView;
	}

	class Holder{

		ImageView iv;
		TextView tv;


	}


}
