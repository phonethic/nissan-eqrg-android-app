package com.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.adapters.TipsContactListViewAdapter.Holder;
import com.phonethics.nissanqrg.R;


public class NissaneQRGListAdapter extends NissanAdapter {

	private Context mContext;
	private AdapterData mmAdapterData;
	

	public NissaneQRGListAdapter(Context context) {
		mContext = context;
		mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		mmAdapterData = new AdapterData();
		/* Set adapter count */
		miAdapterCount = mmAdapterData.nissanEqrgListText.length;
	}


	@SuppressLint("NewApi")
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		Holder holder = null;

		if(convertView == null){
			holder = new Holder();
			convertView = mInflater.inflate(R.layout.nissaneqrglistviewlayout, null);

			holder.iv = (ImageView) convertView.findViewById(R.id.ivNissaneqrg);
			holder.tv = (TextView) convertView.findViewById(R.id.tvNissaneqrg);

			if(android.os.Build.VERSION.SDK_INT >= 14)
				holder.tv.setAlpha(0.9f);
			else
				holder.tv.setBackgroundColor(Color.parseColor("#99FFFFFF"));
			
			convertView.setTag(holder);
		}


		holder = (Holder) convertView.getTag();

		holder.iv.setImageResource(mAdapterData.nissanEqrgListImage[position]);
		holder.tv.setText(mmAdapterData.nissanEqrgListText[position]);

		return convertView;
	}

	class Holder{
		ImageView iv;
		TextView tv;
	}

}
