package com.adapters;


import com.log.NissanLog;
import com.phonethics.nissanqrg.R;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class GridViewAdapter extends NissanAdapter {

	private Context mContext;
	private int miScreenNumber;
	

	public GridViewAdapter(Context context, int screenNumber) {
		mContext = context;
		miScreenNumber = screenNumber;
		mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		if(miScreenNumber == 0) {
			miAdapterCount = mAdapterData.landingScreenGridName.length;;
		} else if(miScreenNumber == 1) {
			miAdapterCount = mAdapterData.trafficRulesText.length;
		}
	}

	
	@Override
	public View getView(int position, View view, ViewGroup viewGroup) {
		Holder holder = null;
		if(view == null) {
			
			if(miScreenNumber == 0) {
				view = mInflater.inflate(R.layout.landingscreengridviewlayout, null);
				holder = new Holder();

				view.setBackgroundColor(Color.parseColor(mAdapterData.landingScreenGridColours[position]));
				holder.iv = (ImageView) view.findViewById(R.id.ivLandingScreenGrid);
				holder.tv = (TextView) view.findViewById(R.id.tvLandingScreenGrid);
			} else if(miScreenNumber == 1) {
				view = mInflater.inflate(R.layout.trafficrulesgridviewlayout, null);
				holder = new Holder();

				holder.iv = (ImageView) view.findViewById(R.id.ivTrafficRulesIcon);
			}
			
			view.setTag(holder);
		}
		
		holder = (Holder) view.getTag();
		
		if(miScreenNumber == 0) {
			holder.iv.setImageResource(mAdapterData.landingScreenGridIcons[position]);
			holder.tv.setText(mAdapterData.landingScreenGridName[position]);
		} else if(miScreenNumber == 1) {
			holder.iv.setImageResource(mAdapterData.trafficRulesIcon[position]);
		}

		return view;
	}

	class Holder {
		ImageView iv;
		TextView tv;
	}

}
