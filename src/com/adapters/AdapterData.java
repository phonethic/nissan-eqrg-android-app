package com.adapters;

import java.util.ArrayList;

import com.log.NissanLog;
import com.phonethics.nissanqrg.R;



public class AdapterData {

	/*
	 * GRID VIEW ADAPTER DATA
	 */
	String[] landingScreenGridColours = {
			"#92cf68", "#58b5e1", "#fcc953",
			"#fa6567", "#f7774a", "#c066a6", 
			"#b25008"	
	};

	int[] landingScreenGridIcons = {
			R.drawable.nissaneqrg, R.drawable.findmycar, 
			R.drawable.reminders, R.drawable.trafficrules, 
			R.drawable.tips, R.drawable.mycar, 
			R.drawable.contactus
	};

	String[] landingScreenGridName = {
			"Nissan eQRG", "Find My Car", 
			"Reminders", "Traffic Rules", 
			"Tips", "My Car", 
			"Contact Us"
	};

	public Integer[] trafficRulesIcon = {
			R.drawable.pic_1, R.drawable.pic_2,
			R.drawable.pic_3, R.drawable.pic_4,
			R.drawable.pic_5, R.drawable.pic_6,
			R.drawable.pic_7, R.drawable.pic_8,
			R.drawable.pic_9, R.drawable.pic_10,
			R.drawable.pic_11, R.drawable.pic_12,
			R.drawable.pic_13, R.drawable.pic_14,
			R.drawable.pic_15,R.drawable.pic_16,
			R.drawable.pic_17,R.drawable.pic_18,
			R.drawable.pic_19,R.drawable.pic_20,
			R.drawable.pic_21,R.drawable.pic_22,
			R.drawable.pic_23,R.drawable.pic_24,
			R.drawable.pic_25,R.drawable.pic_26,
			R.drawable.pic_27,R.drawable.pic_28,
			R.drawable.pic_29,R.drawable.pic_30,
			R.drawable.pic_31,R.drawable.pic_32,
			R.drawable.pic_33,R.drawable.pic_34,
			R.drawable.pic_35,R.drawable.pic_36,
			R.drawable.pic_37,R.drawable.pic_38,
			R.drawable.pic_39,R.drawable.pic_40,
			R.drawable.pic_41,R.drawable.pic_42,
			R.drawable.pic_43,R.drawable.pic_44,
			R.drawable.pic_45,R.drawable.pic_46,
			R.drawable.pic_47,R.drawable.pic_48,
			R.drawable.pic_49,R.drawable.pic_50,
			R.drawable.pic_51,R.drawable.pic_52,
			R.drawable.pic_53,
	};
	public String[] trafficRulesText = {
			"Straight No Entry", "No Parking",
			"No Stopping or Standing","Horns Prohibited",
			"School Ahead", "Narrow Road Ahead",
			"Narrow Bridge", "Right Hand Curve",
			"Left Hand Curve", "Compulsory Sound Horn",
			"One Way Signs" , "One Way Signs",
			"Vehicles Prohibited In Both Directions", "All Motor Vehicles Prohibited",
			"Tonga Prohibited", "Truck Prohibited",
			"Bullock Cart And Hand Cart Prohibited", "Hand Cart Prohibited",
			"Bullock Cart Prohibited", "Cycle Prohibited",
			"Pedestrian Prohibited", "Right Turn Prohibited",
			"Left Turn Prohibited", "U-turn Prohibited",
			"Speed Limit", "Width Limit",
			"Height Limit", "Length Limit",
			"Load Limit", "Axe Load Limit",
			"Bus Stop", "Restriction End Sign",
			"Compulsory Keep Left", "Compulsory Turn Left",
			"Compulsory Turn Right Ahead", "Compulsory Ahead or Turn Left",
			"Compulsory Ahead or Turn Right", "Compulsory Ahead Only",
			"Compulsory Cycle Track" , "Move On",
			"Stop", "Give way",
			"Bump ahead", "Ferry",
			"Right HairPin Bend", "Left HairPin Bend",
			"Left Reverse Bend", "Right Reverse Bend",
			"Steep Ascent" , "Steep Descent",
			"Road Widens Ahead", "Slippery Ahead", "Cycle Crossing"
	};


	/*
	 * VIEW PAGER ADAPTER DATA
	 */
	int[] landingScreenPagerImage = {
			R.drawable.nissan3, R.drawable.nissan4,
			R.drawable.nissan3, R.drawable.nissan4,
			R.drawable.nissan3, R.drawable.nissan4
	};

	String[] landingScreenPagerText = {
			"Dynamic Exteriors", "Premium Interiors",
			"Advance Features", "Dependable Safety",
			"Gallery", "TV Commercials"
	};

	String[] pagerTypes = {
			"Dynamic Exteriors", "Premium Interiors", "Advance Features",
			"Dependable Safety", "Gallery", "TV Commercials"
	};


	/*
	 * LIST VIEW ADAPTER DATA
	 */
	int[] nissanEqrgListImage = {
			R.drawable.nissan4, R.drawable.nissan3,
			R.drawable.nissan4
	};

	String[] nissanEqrgListText = {
			"Nissan Terrano", "Nissan Micra",
			"Nissan Sunny"
	};

	String[] tipsListData = {
			"Maintainance", "Driving Tips", "Location Based Tips",	
			"Season Based Tips", "Safety", "'How To' Videos'"
	};
	int[] tipsListIcon = {
			R.drawable.settings, R.drawable.mycar, R.drawable.locationbasedtips,
			R.drawable.nissaneqrg, R.drawable.safety, R.drawable.howtovideos
	};

	String[] contactUsText = {
			"TALK TO US", 
			"Book a Test Drive", "Call Nissan Customer Support", "Call Breakdown Helpline",

			"EMAIL US",
			"Email Customer Support", "Email Breakdon Helpline",

			"CONNECT VIA",
			"Facebook Connect", "Dealer Locator"
	};
	int[] contactUsIcon = {
			R.drawable.ic_launcher, R.drawable.nissaneqrg, R.drawable.contactus, R.drawable.safety,

			R.drawable.ic_launcher,R.drawable.howtovideos, R.drawable.mycar,

			R.drawable.ic_launcher,R.drawable.facebook, R.drawable.locationbasedtips
	};

	String[] eQRGFeatureList = {
			"Instrument Panel & Control",
			"Know your drive Computer", "Adjustments", 
			"Setting Clock in Combimeter",
			"Setting Clock in audio System", 
			"Heating, ventilation and air conditioning system",
			"Opening Controls"
	};

	private String sectionName;
	private String thumbLink;

	ArrayList<String> sectionNameAr = new ArrayList<String>();
	ArrayList<String> thumbLinkAr = new ArrayList<String>();;

	public String getSectionName() {
		return sectionName;
	}
	public void setSectionName(String sectionName) {
		this.sectionName = sectionName;
		sectionNameAr.add(sectionName);
		NissanLog.nissanLogV("NAMES","NAMES AD" +sectionName);
	}
	public String getThumbLink() {
		return thumbLink;
	}
	public void setThumbLink(String thumbLink) {
		this.thumbLink = thumbLink;
		thumbLinkAr.add(thumbLink);
	}


}
