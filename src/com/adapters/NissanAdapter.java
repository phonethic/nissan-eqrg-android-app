package com.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

public abstract class NissanAdapter extends BaseAdapter {
	
	protected AdapterData mAdapterData = new AdapterData();
	protected int miAdapterCount;
	protected LayoutInflater mInflater;

	@Override
	public int getCount(){
		return miAdapterCount;
	}
	
	
	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}


	@Override
	public abstract View getView(int position, View convertView, ViewGroup parent);

}
