package com.log;

import android.util.Log;

public class NissanLog {
	
	static boolean showLog = true;
	
	/**
	 * Logs an error
	 * @param tag - Tag for Error message
	 * @param message - Error message
	 */
	public static void nissanLogE(String tag, String message){
		if(showLog)
		Log.e(tag, message);
	}

	
	/**
	 * Logs an Information
	 * @param tag - Tag for Information message
	 * @param message - Information message
	 */
	public static void nissanLogI(String tag, String message){
		Log.i(tag, message);
	}
	
	
	/**
	 * Logs a verbose
	 * @param tag - Tag for verbose message
	 * @param message - verbose message
	 */
	public static void nissanLogV(String tag, String message){
		Log.v(tag, message);
	}
}
