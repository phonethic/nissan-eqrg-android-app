package com.database;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.log.NissanLog;

public class DatabaseUtils {

	private final Context ourContext;
	private static final String DATABASE_NAME="NissanDataBase.db";
	private static final int  DATABASE_VERSOIN=1;

	private DbHelper dbHelper;
	private SQLiteDatabase sqlDataBase;

	/**
	 * 
	 * The following variables are for REMEINDERS table........
	 * 
	 * 
	 */
	private final static String table_reminder	= "REMEINDERS";
	private final static String	remId			= "REM_ID";
	private final static String	onoff			= "REMINDERONOFF";
	private final static String	messege_rem		= "MESSEGE";
	private final static String	date_rem		= "DATE";
	private final static String time_rem		= "TIME";
	private final static String	completed		= "COMPLETED";
	private final static String preValue		= "PRE_VALUE";
	private final static String preUnit			= "PRE_UNIT";


	public DatabaseUtils(Context c)
	{
		ourContext=c;
	}


	private static class  DbHelper extends SQLiteOpenHelper
	{

		public DbHelper(Context context) {
			super(context, DATABASE_NAME, null, DATABASE_VERSOIN);
			// TODO Auto-generated constructor stub
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			// TODO Auto-generated method stub

			/*
			 * REMINDERS TABLE
			 */
			String create_table_remeinders = "CREATE TABLE IF NOT EXISTS " + table_reminder + 
					"(" + remId + " INTEGER PRIMARY KEY AUTOINCREMENT," + 
					onoff		    + " TEXT, " + 
					messege_rem		+ " TEXT, " + 
					date_rem		+ " TEXT, " +
					time_rem		+ " TEXT, " +
					completed	    + " TEXT, " +
					preValue	    + " TEXT, " +
					preUnit	    	+ " TEXT" + ");";

			db.execSQL(create_table_remeinders);

		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			// TODO Auto-generated method stub

			db.execSQL("DROP TABLE IF EXISTS " +table_reminder);
			onCreate(db);
		}


	}

	public  DatabaseUtils open() throws SQLException
	{
		dbHelper=new DbHelper(ourContext);
		sqlDataBase=dbHelper.getWritableDatabase();

		return this;

	}

	public void close()
	{
		dbHelper.close();
	}


	/**
	 * Creating an entry for Reminders
	 */

	public void addReminder(String rMessage, String rDate, String rTime, String rPreValue, String rPreUnit){

		ContentValues values = new ContentValues();

		values.put(messege_rem, rMessage);
		values.put(date_rem, 	rDate);
		values.put(time_rem, 	rTime);
		values.put(onoff,   "1");
		values.put(completed, "0");
		values.put(preValue, rPreValue);
		values.put(preUnit, rPreUnit);

		try{

			sqlDataBase.insert(table_reminder, null, values);
			NissanLog.nissanLogV("********Data**********", "----" + "----- "+ rMessage + " " + rDate +" " + rTime + " " + rPreValue + " " + rPreUnit );

		}catch(Exception e){
			NissanLog.nissanLogE(" REMEINDER DB ERROR ", e.toString());
			e.printStackTrace();
		}

	}


	/*
	 *   Getting all of the pending reminders
	 */

	///// will return all pending reminders
	public ArrayList<ArrayList<Object>> getAllPendingReminders(){

		ArrayList<ArrayList<Object>> reminder_Arrays = new ArrayList<ArrayList<Object>>();
		Cursor cursor;
		String [] columns= new String[]{remId,onoff,messege_rem,date_rem,time_rem};
		String query = "SELECT * FROM " + table_reminder + " WHERE " + completed+"=0";
		try{
			/*cursor = db.query(table_reminder,columns,null,null,null,null,null);*/

			cursor = sqlDataBase.rawQuery(query, null);


			int iRemId			=	cursor.getColumnIndex(remId);
			int iOnOff			=	cursor.getColumnIndex(onoff);
			int iMessage		=	cursor.getColumnIndex(messege_rem);

			int iDate			=	cursor.getColumnIndex(date_rem);
			int iTime			=	cursor.getColumnIndex(time_rem);

			int iPreValue		=	cursor.getColumnIndex(preValue);
			int iPreUnit		=	cursor.getColumnIndex(preUnit);
			int iCompleted		=	cursor.getColumnIndex(completed);



			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext()){
				ArrayList<Object> reminderDetails = new ArrayList<Object>();

				reminderDetails.add(cursor.getString(iRemId));
				reminderDetails.add(cursor.getString(iOnOff));
				reminderDetails.add(cursor.getString(iMessage));

				reminderDetails.add(cursor.getString(iDate));
				reminderDetails.add(cursor.getString(iTime));

				reminderDetails.add(cursor.getString(iPreValue));
				reminderDetails.add(cursor.getString(iPreUnit));

				reminderDetails.add(cursor.getString(iCompleted));

				reminder_Arrays.add(reminderDetails);

			}
			cursor.close();
		}catch (SQLException e) {
			Log.e("DB ERROR", e.toString());
			e.printStackTrace();
		}
		return reminder_Arrays;
	}

	//Getting all Completed Reminders
	public ArrayList<ArrayList<Object>> getAllCompletedRemeinders(){

		ArrayList<ArrayList<Object>> reminder_Arrays = new ArrayList<ArrayList<Object>>();
		Cursor cursor;
		String [] columns= new String[]{remId,onoff,messege_rem,date_rem,time_rem};
		String query = "SELECT * FROM " + table_reminder + " WHERE " + completed+"=1";
		try{
			/*cursor = db.query(table_reminder,columns,null,null,null,null,null);*/

			cursor = sqlDataBase.rawQuery(query, null);


			int iRemId		=	cursor.getColumnIndex(remId);
			int iOnOff		=	cursor.getColumnIndex(onoff);
			int iMessage	=	cursor.getColumnIndex(messege_rem);

			int iDate		=	cursor.getColumnIndex(date_rem);
			int iTime		=	cursor.getColumnIndex(time_rem);

			int iPreValue		=	cursor.getColumnIndex(preValue);
			int iPreUnit		=	cursor.getColumnIndex(preUnit);

			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext()){
				ArrayList<Object> reminderDetails = new ArrayList<Object>();

				reminderDetails.add(cursor.getString(iRemId));
				reminderDetails.add(cursor.getString(iOnOff));
				reminderDetails.add(cursor.getString(iMessage));
				reminderDetails.add(cursor.getString(iDate));
				reminderDetails.add(cursor.getString(iTime));
				reminderDetails.add(cursor.getString(iPreValue));
				reminderDetails.add(cursor.getString(iPreUnit));

				reminder_Arrays.add(reminderDetails);

			}
			cursor.close();
		}catch (SQLException e) {
			Log.e("DB ERROR", e.toString());
			e.printStackTrace();
		}
		return reminder_Arrays;
	}


	// update alarm to completed

	public void setAlarm(int id, String comp){

		ContentValues values = new ContentValues();
		//		if(on == 1){
		//			values.put(onoff, "1");
		//		}else if(on == 0){
		//			values.put(onoff, "0");
		//		}
		values.put(completed, comp);

		Log.d("Value Set","Value Set " + comp);

		try{
			sqlDataBase.update(table_reminder, values, remId + "=" + id, null);	

		}catch(Exception e){
			Log.e(" Move to  Complete Error", e.toString());
			e.printStackTrace();
		}
	}

	//Delete reminder from database

	public void deleteReminder(int Id){
		try{
			NissanLog.nissanLogV("Deleted id","Deleted id " + Id);
			sqlDataBase.delete(table_reminder, remId + "=" + Id, null);
		}catch(SQLException ex){
			Log.d("DELETE REMINDER SQL EXCEPTION", ex.toString());
		}catch(Exception ex){
			Log.d("DELETING REMINDER ERROR", ex.toString());
		}

	}
}
